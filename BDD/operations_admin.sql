---------------------------------------------------------------------------------
-- Auteur :		Sébastien Guiwarch
-- Date création 			:	16/06/2016
-- Dernière modificcation	:	16/06/2016
---------------------------------------------------------------------------------
-- Description / objectif : 	Ce script SQL effectue une transaction sur la base de données courante 
--					pour créer la structure (Tables, contraintes, index ...).
--					Le fichier de ce script est encodé en UTF8 
-- Utilisation : 	*	Ce script est à lancer en étant connecté avec un utilisateur admin et sur une base précedemment créée à l'aide du script suivant :
--				https://svngeodb.nancy.inra.fr/svn/scripts/trunk/serveurs%20virtuels/pggeodb/exploitation/creer_base.sh
--			* 	Le compte à utiliser doit avoir les privilèges suffisants pour créer des tables, des index et contraintes ...
--			*	L'utilisation d'une variable psql interpolée dans le code sql (\set) ne fonctionne qu'avec l'outil psql qu'il conviendra
--				d'utiliser pour exécuter ce script (à défaut le script doit être modifié préalablement pour une conformité au SQL
--				classique.
--			* SYNTAXE : psql -f operations_admin.sql nombase
-- PARAMETRES :
--		en entrée :
-- 			aucun
--		en sortie :
-- 	

--\set grpecriture 'symphonie_ecriture'
\set grplecture 'symphonie_lecture'
\set grpapp 'symphonie_app'

-- L'utilisation d'une transaction permet de créer toute la structure ou rien si une erreur survient, laissant la base de données dans un état stable
START TRANSACTION;

-- ****************************************
-- ***** Création des extensions      *****
-- ****************************************

	--CREATE extension postgres_fdw;

-- ************************************************************************
-- ***** Création du schéma symphonie *****
-- ************************************************************************

	--CREATE SCHEMA symphonie;
	--ALTER SCHEMA symphonie OWNER TO symphonie;
	--GRANT USAGE ON SCHEMA symphonie TO :grplecture;
	--GRANT ALL ON SCHEMA symphonie TO :grpecriture WITH GRANT OPTION;
	


-- ******************************************
-- ***** Modification du role symphonie *****
-- ******************************************

-- Modification du role symphonie
	--ALTER ROLE symphonie 
	--WITH
	--	SUPERUSER;
		
	--ALTER GROUP :grpecriture ADD USER symphonie;
	
-- ************************************************************************
-- ***** Création du role utilisé pour se connecter via l'application *****
-- ************************************************************************
-- Création du rôle symphonie_app
	CREATE ROLE :grpapp
	WITH
		LOGIN
		ENCRYPTED PASSWORD 'tata' --'@=?MwVfyFu92dR6B7@E6#2X2)cPi8b35ypP(_4=8H*(9XE$sag'
		INHERIT
		IN GROUP :grpecriture;
		
COMMIT;	-- Enregistre la totalité du traitement si aucune erreur n'est survenue
		
