﻿-- ******************************* INFORMATIONS ************************************** --
-- *** 1). Pour lancer ce script il faut être connecté à la base de donnée SAMPLES *** --
-- *** 	   du serveur CHACO (Orléans) et être connecté avec un role disposant      *** --
-- *** 	   des droits pour créer des rôles et modifier des permissions.            *** --
-- *********************************************************************************** --

-- L'utilisation d'une transaction permet de créer toute la structure ou rien si une erreur survient, laissant la base de données dans un état stable
START TRANSACTION;

-- ********************************************
-- ***** Création du role symphonie_venik *****
-- ********************************************
	CREATE ROLE symphonie_venik 
	WITH
		LOGIN
		ENCRYPTED PASSWORD '%pgFbKZf8?6G_hn5AZ@W$?p58>Ysc2k362Jf3D=94yZM$5?f%:'
		IN GROUP veniklec2;

-- ********************************************
-- **** Création du groupe samples_lecture ****
-- ********************************************
	CREATE ROLE samples_lecture
	WITH
		NOLOGIN;

-- ********************************************
-- **** Création du role symphonie_samples ****
-- ********************************************
	CREATE ROLE symphonie_samples 
	WITH
		LOGIN
		ENCRYPTED PASSWORD 'PE=wz52md8<D47JjzW3!?i5LN-f~yC:4G9ed2:U5m2%T>4_M=f'
		IN GROUP samples_lecture;

-- ***************************************************************************************************************
-- ***** Affectation des droits de lecture sur le schéma data, ainsi que sur toutes les tables de ce schéma. *****
-- ***************************************************************************************************************
	GRANT USAGE ON SCHEMA data TO samples_lecture;
	GRANT SELECT ON ALL TABLES IN SCHEMA data TO samples_lecture;
	--GRANT SELECT ON data.analysis, data.barcode, data.destruction, data.dico_externe_element, data.dico_externe_type, data.dico_interne_element, data.dico_interne_type, data.file, data.operator, data.origin_shortcut, data.project, data.reception, data.relationship, data.sample, data.sample_file, data.sample_initial, data.sender, data.terrain_paper, data.treatment TO samples_lecture;

COMMIT -- Enregistre la totalité du traitement si aucune erreur n'est survenue