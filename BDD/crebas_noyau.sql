﻿---------------------------------------------------------------------------------
-- Auteur :		Sébastien Guiwarch
-- Date création 			:	23/03/2018
-- Dernière modificcation	:	23/03/2018
---------------------------------------------------------------------------------
-- Description / objectif : 	Ce script SQL crée le schéma noyau au sein de la base utilisée, puis créé au sein de ce schéma différentes vues permettant l'utilisation du module noyau des 
--								formulaires Access.
-- Utilisation : 	*	Ce script est à lancer en étant connecté avec un utilisateur admin et sur une base précedemment créée à l'aide du script suivant :
--			* 	Le compte à utiliser doit avoir les privilèges suffisants pour créer des schémas, des foreign table et vues ...
--			*	L'utilisation d'une variable psql interpolée dans le code sql (\set) ne fonctionne qu'avec l'outil psql qu'il conviendra
--				d'utiliser pour exécuter ce script (à défaut le script doit être modifié préalablement pour une conformité au SQL
--				classique.
--			* SYNTAXE : psql -f crebas_noyau.sql -U [USER_LOGIN] -d [NOM_BASE]
-- PARAMETRES :
--		en entrée :
-- 			aucun
--		en sortie :
--

\set proprietaire 'symphonie'
\set grpecriture 'symphonie_ecriture'
\set grplecture 'symphonie_lecture'
\set grpconnect 'symphonie_connect'

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;
SET search_path = public, pg_catalog;
SET default_tablespace = '';

-- L'utilisation d'une transaction permet de créer toute la structure ou rien si une erreur survient, laissant la base de données dans un état stable
START TRANSACTION;

-- ****************************************
-- ***** Création des extensions      *****
-- ****************************************

	CREATE EXTENSION IF NOT EXISTS postgres_fdw;

-- ****************************************
-- ***** Création des schémas         *****
-- ****************************************

	-- Schéma noyau
	CREATE SCHEMA IF NOT EXISTS noyau;
	ALTER SCHEMA noyau owner TO :proprietaire;
	GRANT USAGE ON SCHEMA noyau TO :grplecture;
	GRANT ALL ON SCHEMA noyau TO :grpecriture WITH GRANT OPTION;

	-- ************************************************************************
-- ***** ModificatiON des droits par défaut lors de la création dobjet ****
-- ************************************************************************

	ALTER DEFAULT PRIVILEGES
		IN SCHEMA noyau
		GRANT SELECT
		ON tables
		TO :grplecture;

-- ****************************************
-- ***** création des foreign serveurs  ***
-- ****************************************

	-- SERVER server_noyausymphonie
	CREATE SERVER server_noyausymphonie
	FOREIGN DATA WRAPPER postgres_fdw 
	OPTIONS (dbname 'db_noyausymphonie', port '5432', host '192.168.58.150');
	ALTER SERVER server_noyausymphonie OWNER TO :proprietaire;
	GRANT USAGE ON FOREIGN SERVER server_noyausymphonie TO :grpconnect;

-- **********************************************
-- ***** création du mapping des utilisateurs ***
-- ***** sur le serveur serveur_pggeodb       ***
-- **********************************************

	-- USER MAPPING noyausymphonie
	CREATE USER MAPPING FOR :proprietaire
	SERVER server_noyausymphonie
	OPTIONS (user 'noyausymphonie', password '2p5XyR9Yb');

-- ************************************************************
-- ***** création des tables étrangères db_noyausymphonie *****
-- ************************************************************

	-- TABLE : tf_noyau_composite_node_data_set
	CREATE FOREIGN TABLE noyau.tf_noyau_composite_node_data_set (
		branch_node_id	BIGINT	NOT NULL,
		index			INTEGER	NOT NULL,
		id_ancestor		BIGINT,
		id_parent_node	BIGINT,
		realnode		BIGINT	NOT NULL
	)SERVER server_noyausymphonie OPTIONS (schema_name 'public', table_name 'composite_node_data_set');
	ALTER TABLE noyau.tf_noyau_composite_node_data_set OWNER TO :proprietaire;

	-- TABLE : tf_noyau_composite_node_ref_data
	CREATE FOREIGN TABLE noyau.tf_noyau_composite_node_ref_data (
		branch_node_id	BIGINT	NOT NULL,
		index			INTEGER	NOT NULL,
		id_ancestor		BIGINT,
		id_parent_node	BIGINT,
		realnode		BIGINT	NOT NULL
	)SERVER server_noyausymphonie OPTIONS (schema_name 'public', table_name 'composite_node_ref_data');
	ALTER TABLE noyau.tf_noyau_composite_node_ref_data OWNER TO :proprietaire;

	-- TABLE : tf_noyau_composite_nodeable
	CREATE FOREIGN TABLE noyau.tf_noyau_composite_nodeable (
		dtype		CHARACTER VARYING(31)	NOT NULL,
		id			BIGINT					NOT NULL,
		code		CHARACTER VARYING(255)	NOT NULL,
		ordernumber	BIGINT
	)SERVER server_noyausymphonie OPTIONS (schema_name 'public', table_name 'composite_nodeable');
	ALTER TABLE noyau.tf_noyau_composite_nodeable OWNER TO :proprietaire;

	-- TABLE : tf_noyau_datatype
	CREATE FOREIGN TABLE noyau.tf_noyau_datatype (
		description	TEXT,
		name		CHARACTER VARYING(255)	NOT NULL,
		dty_id		BIGINT					NOT NULL
	)SERVER server_noyausymphonie OPTIONS (schema_name 'public', table_name 'datatype');
	ALTER TABLE noyau.tf_noyau_datatype OWNER TO :proprietaire;

	-- TABLE : tf_noyau_filetype
	CREATE FOREIGN TABLE noyau.tf_noyau_filetype (
		ft_id		BIGINT					NOT NULL,
		code		CHARACTER VARYING(255)	NOT NULL,
		description	TEXT,
		name		CHARACTER VARYING(255)	NOT NULL
	)SERVER server_noyausymphonie OPTIONS (schema_name 'public', table_name 'filetype');
	ALTER TABLE noyau.tf_noyau_filetype OWNER TO :proprietaire;

	-- TABLE : tf_noyau_group_filecomp
	CREATE FOREIGN TABLE noyau.tf_noyau_group_filecomp (
		group_id		BIGINT	NOT NULL,
		file_comp_id	BIGINT	NOT NULL
	)SERVER server_noyausymphonie OPTIONS (schema_name 'public', table_name 'group_filecomp');
	ALTER TABLE noyau.tf_noyau_group_filecomp OWNER TO :proprietaire;

	-- TABLE : tf_noyau_group_utilisateur
	CREATE FOREIGN TABLE noyau.tf_noyau_group_utilisateur (
		usr_id		BIGINT	NOT NULL,
		group_id	BIGINT	NOT NULL
	)SERVER server_noyausymphonie OPTIONS (schema_name 'public', table_name 'group_utilisateur');
	ALTER TABLE noyau.tf_noyau_group_utilisateur OWNER TO :proprietaire;

	-- TABLE : tf_noyau_groupe
	CREATE FOREIGN TABLE noyau.tf_noyau_groupe (
		id					BIGINT					NOT NULL,
		activityasstring	TEXT,
		group_name			CHARACTER VARYING(255),
		group_which_tree	CHARACTER VARYING(255)
	)SERVER server_noyausymphonie OPTIONS (schema_name 'public', table_name 'groupe');
	ALTER TABLE noyau.tf_noyau_groupe OWNER TO :proprietaire;

	-- TABLE : tf_noyau_insertion_dataset_ids
	CREATE FOREIGN TABLE noyau.tf_noyau_insertion_dataset_ids (
		ids_id					BIGINT						NOT NULL,
		ids_create_date			TIMESTAMP WITHOUT TIME ZONE NOT NULL,
		ids_date_debut_periode	TIMESTAMP WITHOUT TIME ZONE	NOT NULL,
		ids_date_fin_periode	TIMESTAMP WITHOUT TIME ZONE NOT NULL,
		ids_publish_comment		CHARACTER VARYING(255),
		ids_publish_date		TIMESTAMP WITHOUT TIME ZONE,
		ids_create_user			BIGINT						NOT NULL,
		ids_publish_user		BIGINT,
		ivf_id					BIGINT,
		id						BIGINT						NOT NULL
	)SERVER server_noyausymphonie OPTIONS (schema_name 'public', table_name 'insertion_dataset_ids');
	ALTER TABLE noyau.tf_noyau_insertion_dataset_ids OWNER TO :proprietaire;

	-- TABLE : tf_noyau_insertion_filecomp_ifcs
	CREATE FOREIGN TABLE noyau.tf_noyau_insertion_filecomp_ifcs (
		ifcs_id					BIGINT						NOT NULL,
		ifcs_code				CHARACTER VARYING(255)		NOT NULL,
		contenttype				CHARACTER VARYING(255),
		ifcs_create_date		TIMESTAMP WITHOUT TIME ZONE,
		data					OID							NOT NULL,
		ifcs_date_debut_periode	DATE,
		ifcs_date_fin_periode	DATE,
		ifsc_description		TEXT						NOT NULL,
		ifcs_name				CHARACTER VARYING(255)		NOT NULL,
		forapplication			BOOLEAN,
		ifcs_last_modify_date	TIMESTAMP WITHOUT TIME ZONE,
		mandatory				BOOLEAN,
		ivfc_size				INTEGER,
		ifcs_create_user		BIGINT						NOT NULL,
		ft_id					BIGINT						NOT NULL,
		ifcs_last_modify_user	BIGINT						NOT NULL
	)SERVER server_noyausymphonie OPTIONS (schema_name 'public', table_name 'insertion_filecomp_ifcs');
	ALTER TABLE noyau.tf_noyau_insertion_filecomp_ifcs OWNER TO :proprietaire;

	-- TABLE : tf_noyau_insertion_version_file_ivf
	CREATE FOREIGN TABLE noyau.tf_noyau_insertion_version_file_ivf (
		ivf_id BIGINT NOT NULL,
		data oid,
		extention			CHARACTER VARYING(255),
		filename			CHARACTER VARYING(255),
		ivf_size			INTEGER,
		ivf_upload_date		TIMESTAMP WITHOUT TIME ZONE	NOT NULL,
		ivf_version_number	BIGINT,
		ids_id				BIGINT						NOT NULL,
		ivf_upload_user		BIGINT						NOT NULL
	)SERVER server_noyausymphonie OPTIONS (schema_name 'public', table_name 'insertion_version_file_ivf');
	ALTER TABLE noyau.tf_noyau_insertion_version_file_ivf OWNER TO :proprietaire;

	-- TABLE : tf_noyau_realnode
	CREATE FOREIGN TABLE noyau.tf_noyau_realnode (
		id				BIGINT					NOT NULL,
		path			CHARACTER VARYING(255),
		id_ancestor		BIGINT,
		id_nodeable		BIGINT,
		id_parent_node	BIGINT
	)SERVER server_noyausymphonie OPTIONS (schema_name 'public', table_name 'realnode');
	ALTER TABLE noyau.tf_noyau_realnode OWNER TO :proprietaire;

	-- TABLE : tf_noyau_utilisateur
	CREATE FOREIGN TABLE noyau.tf_noyau_utilisateur (
		id			BIGINT					NOT NULL,
		active		BOOLEAN					NOT NULL,
		email		CHARACTER VARYING(255)	NOT NULL,
		emploi		CHARACTER VARYING(255),
		isroot		BOOLEAN,
		language	CHARACTER VARYING(255),
		login		CHARACTER VARYING(255)	NOT NULL,
		nom			CHARACTER VARYING(255)	NOT NULL,
		password	CHARACTER VARYING(255)	NOT NULL,
		poste		CHARACTER VARYING(255),
		prenom		CHARACTER VARYING(255)	NOT NULL
	)SERVER server_noyausymphonie OPTIONS (schema_name 'public', table_name 'utilisateur');
	ALTER TABLE noyau.tf_noyau_utilisateur OWNER TO :proprietaire;

-- *******************************************************************
-- ***** création des vues concernant les FOREIGN TABLE dy noyau   ***
-- *******************************************************************

	-- vue : vf_noyau_composite_node_data_set
	CREATE VIEW noyau.vf_noyau_composite_node_data_set AS
		SELECT * 
		FROM noyau.tf_noyau_composite_node_data_set;
	
	ALTER VIEW noyau.vf_noyau_composite_node_data_set OWNER TO :proprietaire;

	-- vue : vf_noyau_composite_node_ref_data
	CREATE VIEW noyau.vf_noyau_composite_node_ref_data AS
		SELECT * 
		FROM noyau.tf_noyau_composite_node_ref_data;
	
	ALTER VIEW noyau.vf_noyau_composite_node_ref_data OWNER TO :proprietaire;

	-- vue : vf_noyau_composite_nodeable
	CREATE VIEW noyau.vf_noyau_composite_nodeable AS
		SELECT * 
		FROM noyau.tf_noyau_composite_nodeable;
	
	ALTER VIEW noyau.vf_noyau_composite_nodeable OWNER TO :proprietaire;

	-- vue : vf_noyau_datatype
	CREATE VIEW noyau.vf_noyau_datatype AS
		SELECT * 
		FROM noyau.tf_noyau_datatype;
	
	ALTER VIEW noyau.vf_noyau_datatype OWNER TO :proprietaire;

	-- vue : vf_noyau_filetype
	CREATE VIEW noyau.vf_noyau_filetype AS
		SELECT * 
		FROM noyau.tf_noyau_filetype;
	
	ALTER VIEW noyau.vf_noyau_filetype OWNER TO :proprietaire;

	-- vue vf_noyau_group_filecomp
	CREATE VIEW noyau.vf_noyau_group_filecomp AS
		SELECT * 
		FROM noyau.tf_noyau_group_filecomp;
	
	ALTER VIEW noyau.vf_noyau_group_filecomp OWNER TO :proprietaire;

	-- vue : vf_noyau_group_utilisateur
	CREATE VIEW noyau.vf_noyau_group_utilisateur AS
		SELECT * 
		FROM noyau.tf_noyau_group_utilisateur;
	
	ALTER VIEW noyau.vf_noyau_group_utilisateur OWNER TO :proprietaire;

	-- vue : vf_noyau_groupe
	CREATE VIEW noyau.vf_noyau_groupe AS
		SELECT * 
		FROM noyau.tf_noyau_groupe;
	
	ALTER VIEW noyau.vf_noyau_groupe OWNER TO :proprietaire;

	-- vue : vf_noyau_insertion_dataset_ids
	CREATE VIEW noyau.vf_noyau_insertion_dataset_ids AS
		SELECT * 
		FROM noyau.tf_noyau_insertion_dataset_ids;
	
	ALTER VIEW noyau.vf_noyau_insertion_dataset_ids OWNER TO :proprietaire;

	-- vue : vf_noyau_insertion_filecomp_ifcs
	CREATE VIEW noyau.vf_noyau_insertion_filecomp_ifcs AS
		SELECT * 
		FROM noyau.tf_noyau_insertion_filecomp_ifcs;
	
	ALTER VIEW noyau.vf_noyau_insertion_filecomp_ifcs OWNER TO :proprietaire;

	-- vue : vf_noyau_insertion_version_file_ivf
	CREATE VIEW noyau.vf_noyau_insertion_version_file_ivf AS
		SELECT * 
		FROM noyau.tf_noyau_insertion_version_file_ivf;
	
	ALTER VIEW noyau.vf_noyau_insertion_version_file_ivf OWNER TO :proprietaire;

	-- vue : vf_noyau_realnode
	CREATE VIEW noyau.vf_noyau_realnode AS
		SELECT * 
		FROM noyau.tf_noyau_realnode;
	
	ALTER VIEW noyau.vf_noyau_realnode OWNER TO :proprietaire;

	-- vue : vf_noyau_utilisateur
	CREATE VIEW noyau.vf_noyau_utilisateur AS
		SELECT * 
		FROM noyau.tf_noyau_utilisateur;
	
	ALTER VIEW noyau.vf_noyau_utilisateur OWNER TO :proprietaire;

	-- vue : vf_noyau_fichiers
	CREATE VIEW noyau.vf_noyau_fichiers AS
		 SELECT concat(version.filename, '_', to_char(dataset.ids_date_debut_periode, 'DD-mm-YYYY'::text), '_', to_char(dataset.ids_date_fin_periode, 'DD-mm-YYYY'::text)) AS nom,
    version.ivf_version_number AS version,
    datatype.name AS datatype,
    dataset.ids_id AS dataset,
        CASE
            WHEN dataset.ivf_id = version.ivf_id THEN true
            ELSE false
        END AS publie,
        CASE
            WHEN dataset.ivf_id = version.ivf_id THEN dataset.ids_publish_date
            ELSE NULL::timestamp without time zone
        END AS publie_le,
        CASE
            WHEN dataset.ivf_id = version.ivf_id THEN usr.login
            ELSE NULL::character varying
        END AS publie_par,
    version.data AS blob,
        CASE
            WHEN dataset.ivf_id = version.ivf_id THEN usr.id
            ELSE NULL::integer::bigint
        END AS user_id
   FROM noyau.vf_noyau_insertion_dataset_ids dataset
     RIGHT JOIN noyau.vf_noyau_insertion_version_file_ivf version ON version.ids_id = dataset.ids_id
     JOIN noyau.vf_noyau_realnode realnode ON dataset.id = realnode.id
     JOIN noyau.vf_noyau_realnode realnode_parent ON realnode_parent.id = realnode.id_parent_node
     JOIN noyau.vf_noyau_composite_nodeable composite_nodeable ON composite_nodeable.id = realnode_parent.id_nodeable
	 JOIN noyau.vf_noyau_datatype datatype ON composite_nodeable.id = datatype.dty_id
     LEFT JOIN noyau.vf_noyau_utilisateur usr ON dataset.ids_publish_user = usr.id;

	ALTER VIEW noyau.vf_noyau_fichiers OWNER TO :proprietaire;

COMMIT;