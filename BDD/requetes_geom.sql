UPDATE symphonie.t_borne_bor SET geom = ST_Transform(ST_SetSRID(ST_MakePoint(bor_longitude, bor_latitude), 4326),2154)
WHERE bor_refg_id IN (SELECT refg_id FROM symphonie.tr_referentiel_geographique_refg WHERE refg_libelle = 'WGS84 GreenWich décimal (Internationnal)');

UPDATE symphonie.t_borne_bor SET geom = ST_SetSRID(ST_MakePoint(bor_longitude, bor_latitude), 2154) 
WHERE bor_refg_id IN (SELECT refg_id FROM symphonie.tr_referentiel_geographique_refg WHERE refg_libelle = 'Lambert 93 (Métropole + corse)');
