---------------------------------------------------------------------------------
-- Auteur :		Sébastien Guiwarch
-- DATE création 			:	21/12/2015
-- Dernière modificcation	:	21/12/2015
---------------------------------------------------------------------------------
-- description / objectif : 	Ce script SQL effectue une transaction sur la base de données courante 
--					pour créer la structure (tables, contraintes, index ...).
--					Le fichier de ce script est encodé en UTF8 
-- Utilisation : 	*	Ce script est à lancer sur une base précedemment créée à l'aide du script suivant :
--				https://svngeodb.nancy.inra.fr/svn/scripts/trunk/serveurs%20virtuels/pggeodb/exploitation/creer_base.sh
--			* 	Le compte à utiliser doit avoir les privilèges suffisants pour créer des tables, des indexs et contraintes ...
--			*	L'utilisation d'Une variable psql interpolée dans le code sql (\set) ne fonctionne qu'Avec l'outil psql qu'Il conviendra
--				d'utiliser pour exécuter ce script (à défaut le script doit être modifié préalablement pour une conformité au SQL
--				classique.
--			* SYNTAXE : psql -f crebas_symphonie.sql [NOM_BASE] -U [NOM_USER]
-- PARAMETRES :
--		en entrée :
-- 			aucun
--		en sortie :
-- 	

\set proprietaire 'symphonie'
\set grpecriture 'symphonie_ecriture'
\set grplecture 'symphonie_lecture'
\set grpsample 'symphonie_samples'
\set grpvenik 'symphonie_venik'
\set grpapp 'symphonie_connect'

SET client_encoding = 'UTF8';
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET DEFAULT_tablespace = '';

-- L'Utilisation d'Une transaction permet de créer toute la structure ou rien si une erreur survient, laissant la base de données dans un état stable
START TRANSACTION;

-- ****************************************
-- ***** création des schémas         *****
-- ****************************************
	CREATE SCHEMA symphonie;
	ALTER SCHEMA symphonie OWNER TO :proprietaire;
	GRANT USAGE ON SCHEMA symphonie TO :grplecture;
	GRANT ALL ON SCHEMA symphonie TO :grpecriture WITH GRANT OPTION;

-- ****************************************
-- ***** création des extensions      *****
-- ****************************************
	CREATE EXTENSION IF NOT EXISTS postgres_fdw;

-- ****************************************
-- ***** création des foreign serveurs        *****
-- ****************************************
	CREATE SERVER server_samples
	FOREIGN DATA WRAPPER postgres_fdw 
	OPTIONS (dbname 'SAMPLES', port '5432', host '147.99.222.194');
	ALTER SERVER server_samples OWNER TO :proprietaire;
	GRANT USAGE ON FOREIGN SERVER server_samples TO :grpapp;

	CREATE SERVER server_venik
	FOREIGN DATA WRAPPER postgres_fdw 
	OPTIONS (dbname 'Venik', port '5432', host '147.99.222.194');
	ALTER SERVER server_venik OWNER TO :proprietaire;
	GRANT USAGE ON FOREIGN SERVER server_venik TO :grpapp;

-- **********************************************
-- ***** création du mapping des utilisateurs ***
-- ***** sur le serveur serveur_samples       ***
-- **********************************************
	CREATE USER MAPPING FOR :proprietaire
	SERVER server_samples
	OPTIONS (user 'symphonie_samples', password 'PE=wz52md8<D47JjzW3!?i5LN-f~yC:4G9ed2:U5m2%T>4_M=f');

-- **********************************************
-- ***** création du mapping des utilisateurs ***
-- ***** sur le serveur serveur_venik         ***
-- **********************************************
	CREATE USER MAPPING FOR :proprietaire
	SERVER server_venik
	OPTIONS (user 'symphonie_venik', password '%pgFbKZf8?6G_hn5AZ@W$?p58>Ysc2k362Jf3D=94yZM$5?f%:');

-- ************************************************************************
-- ***** Modification des droits par défaut lors de la création dobjet ****
-- ************************************************************************

	ALTER DEFAULT PRIVILEGES
		IN SCHEMA symphonie
		GRANT ALL 
		ON tables
		TO :grpecriture;
	
	ALTER DEFAULT PRIVILEGES
		IN SCHEMA symphonie
		GRANT SELECT
		ON tables
		TO :grplecture;
		
	ALTER DEFAULT PRIVILEGES
		IN SCHEMA symphonie
		GRANT ALL
		ON sequences
		TO :proprietaire, :grpecriture;
		
-- ************************************************
-- ***** création des tables étrangères VENIK *****
-- ************************************************

	-- TABLE : tf_venik_genotype_gen
	CREATE FOREIGN TABLE symphonie.tf_venik_genotype_gen (
		idgeno					INTEGER					NOT NULL,
		idgeno_pere				INTEGER,
		idespece				INTEGER					NOT NULL,
		idlot_pollen_pere		INTEGER,
		idgeno_mere				INTEGER,
		lieu_origine			INTEGER,
		type_geno				INTEGER					NOT NULL,
		nom_arrivee				CHARACTER VARYING(60),
		nom_court				CHARACTER VARYING(30)	NOT NULL,
		date_creation			DATE,
		date_maj				DATE,
		commentaire TEXT,
		programme				CHARACTER VARYING(30),
		annee_arrivee			INTEGER,
		recoltant_exp			CHARACTER VARYING(150),
		proprietaire_legal		CHARACTER VARYING(90),
		autre_lieu				CHARACTER VARYING(128),
		inscription_officielle	CHARACTER VARYING(50),
		mois_arrivee			INTEGER,
		recup_nom_lieu			CHARACTER VARYING(50),
		recup_code_espece		CHARACTER VARYING(50),
		recup_nom_geno			CHARACTER VARYING(50),
		recup_com1 TEXT,
		recup_com2 TEXT,
		recup_pollen			CHARACTER VARYING(50),
		recup_geno_mere			CHARACTER VARYING(50),
		recup_geno_pere			CHARACTER VARYING(50),
		code_crb32				CHARACTER VARYING(50),
		iduser					INTEGER,
		nb_constituants			INTEGER,
		recolte_aire_nat		CHARACTER VARYING(15),
		lettre_type				CHARACTER VARYING(1)	NOT NULL
	)SERVER server_venik OPTIONS (schema_name 'public', table_name 'genotype');
	ALTER TABLE symphonie.tf_venik_genotype_gen OWNER TO :proprietaire;
	
	-- TABLE : tf_venik_espece_esp
	CREATE FOREIGN TABLE symphonie.tf_venik_espece_esp (
		idespece			INTEGER					NOT NULL,
		idgenre				INTEGER					NOT NULL,
		nom					CHARACTER VARYING(100)	NOT NULL,
		mini				NUMERIC,
		maxi				NUMERIC,
		commentaire			CHARACTER VARYING(500),
		date_creation		DATE,
		date_maj			DATE,
		idespece_pere		INTEGER,
		idespece_mere		INTEGER,
		recup_code			CHARACTER VARYING(50),
		code				CHARACTER VARYING(3)	NOT NULL,
		reciproque			CHARACTER VARYING(3),
		espece_crb32		CHARACTER VARYING(10)
	)SERVER server_venik OPTIONS (schema_name 'public', table_name 'espece');
	ALTER TABLE symphonie.tf_venik_espece_esp OWNER TO :proprietaire;
	
	-- TABLE : tf_venik_genre_genr
	CREATE FOREIGN TABLE symphonie.tf_venik_genre_genr (
		idgenre				INTEGER					NOT NULL,
		nom					CHARACTER VARYING(50)	NOT NULL,
		commentaire			CHARACTER VARYING(500),
		date_creation		DATE,
		date_maj			DATE,
		code				CHARACTER VARYING(2)	NOT NULL
	)SERVER server_venik OPTIONS (schema_name 'public', table_name 'genre');
	ALTER TABLE symphonie.tf_venik_genre_genr OWNER TO :proprietaire;
	
	-- TABLE : tf_venik_type_genotype_ref_tgr
	CREATE FOREIGN TABLE symphonie.tf_venik_type_genotype_ref_tgr (
		id_lc			INTEGER NOT NULL,
		lettre_type		CHARACTER VARYING(1)		NOT NULL
	)SERVER server_venik OPTIONS (schema_name 'public', table_name 'type_genotype_ref');
	ALTER TABLE symphonie.tf_venik_type_genotype_ref_tgr OWNER TO :proprietaire;

-- ******************************************************
-- ***** création des vues des FOREIGN TABLE de Venik ***
-- ******************************************************

	-- vue : vf_genotype_vgen
	CREATE VIEW symphonie.vf_genotype_vgen AS
		SELECT * 
		FROM symphonie.tf_venik_genotype_gen;
	
	ALTER VIEW symphonie.vf_genotype_vgen OWNER TO :proprietaire;
		
	-- vue : vf_genre_vgenr
	CREATE VIEW symphonie.vf_genre_vgenr AS
		SELECT * 
		FROM symphonie.tf_venik_genre_genr;
	
	ALTER VIEW symphonie.vf_genre_vgenr OWNER TO :proprietaire;
		
	-- vue : vf_espece_vesp
	CREATE VIEW symphonie.vf_espece_vesp AS
		SELECT * 
		FROM symphonie.tf_venik_espece_esp;
	
	ALTER VIEW symphonie.vf_espece_vesp OWNER TO :proprietaire;
	
	-- vue : vf_type_genotype_ref_vtgr
	CREATE VIEW symphonie.vf_type_genotype_ref_vtgr AS
		SELECT * 
		FROM symphonie.tf_venik_type_genotype_ref_tgr;
	
	ALTER VIEW symphonie.vf_type_genotype_ref_vtgr OWNER TO :proprietaire;

-- ************************************************
-- **** création des tables étrangères samples ****
-- ************************************************

	-- TABLE : tf_samples_samples_sam
	CREATE FOREIGN TABLE symphonie.tf_samples_samples_sam (
		sample_id					INTEGER						NOT NULL,
		barcode_fid					INTEGER						NOT NULL,
		sample_operator_fid			INTEGER,
		project_fid					INTEGER,
		team_fid					INTEGER,
		paper_fid					INTEGER,
		sample_name					CHARACTER VARYING(50),
		nature_entity				CHARACTER VARYING(100),
		destroyed					BOOLEAN						NOT NULL,
		quantity					REAL,
		quantity_use				REAL,
		sample_unit					CHARACTER VARYING(100),
		count_entity				CHARACTER VARYING(100),
		origin_fid					INTEGER,
		genetic_code				CHARACTER VARYING(25),
		is_gm						BOOLEAN						NOT NULL,
		info_is_gm					CHARACTER VARYING(1000),
		age							INTEGER,
		age_unit					CHARACTER VARYING(100),
		conditioning				CHARACTER VARYING(100),
		conditioning_state			CHARACTER VARYING(100),
		date_creation				DATE,
		"comment"					CHARACTER VARYING(1000),
		storage_place_fid			INTEGER,
		store_keeper_fid			INTEGER,
		date_storage				DATE,
		comment_storage				CHARACTER VARYING(1000)
	)SERVER server_samples OPTIONS (schema_name 'data', table_name 'sample');
	ALTER TABLE symphonie.tf_samples_samples_sam OWNER TO :proprietaire;
	
	-- TABLE : tf_samples_barcode_barc
	CREATE FOREIGN TABLE symphonie.tf_samples_barcode_barc (
		barcode_id		INTEGER					NOT NULL,
		code			CHARACTER VARYING(15)	NOT NULL,
		"type"			CHARACTER VARYING(50)	NOT NULL
	)SERVER server_samples OPTIONS (schema_name 'data', table_name 'barcode');
	ALTER TABLE symphonie.tf_samples_barcode_barc OWNER TO :proprietaire;
	
	-- TABLE : tf_samples_operator_ope
	CREATE FOREIGN TABLE symphonie.tf_samples_operator_ope (
		operator_id				INTEGER						NOT NULL,
		name					CHARACTER VARYING(50)		NOT NULL,
		first_name				CHARACTER VARYING(25)		NOT NULL,
		"login"					CHARACTER VARYING(25)		NOT NULL,
		"password"				CHARACTER VARYING(25)		NOT NULL,
		status					CHARACTER VARYING(25),
		present					BOOLEAN						NOT NULL,
		right_group				CHARACTER VARYING(25)		NOT NULL,
		"comment"				CHARACTER VARYING(1000)
	)SERVER server_samples OPTIONS (schema_name 'data', table_name 'operator');
	ALTER TABLE symphonie.tf_samples_operator_ope OWNER TO :proprietaire;
	
	-- TABLE : tf_samples_project_proj
	CREATE FOREIGN TABLE symphonie.tf_samples_project_proj (
		project_id			INTEGER						NOT NULL,
		name				CHARACTER VARYING(100),
		date_beginning		DATE,
		date_ending			DATE,
		"comment"			CHARACTER VARYING(1000)
	)SERVER server_samples OPTIONS (schema_name 'data', table_name 'project');
	ALTER TABLE symphonie.tf_samples_project_proj OWNER TO :proprietaire;
	
	-- TABLE : tf_samples_terrain_paper_tpap
	CREATE FOREIGN TABLE symphonie.tf_samples_terrain_paper_tpap (
		paper_id			INTEGER						NOT NULL,
		barcode_fid			INTEGER						NOT NULL,
		scan				CHARACTER VARYING(100)		NOT NULL
	)SERVER server_samples OPTIONS (schema_name 'data', table_name 'terrain_paper');
	ALTER TABLE symphonie.tf_samples_terrain_paper_tpap OWNER TO :proprietaire;
	
	-- TABLE : tf_samples_dico_intern_element_dicie
	CREATE FOREIGN TABLE symphonie.tf_samples_dico_intern_element_dicie (
		element_id				INTEGER						NOT NULL,
		element_name			CHARACTER VARYING(100)		NOT NULL,
		element_fid				INTEGER						NOT NULL,
		data_type_fid			INTEGER						NOT NULL,
		element_comment			CHARACTER VARYING(1000),
		barcode_fid				INTEGER
	)SERVER server_samples OPTIONS (schema_name 'data', table_name 'dico_interne_element');
	ALTER TABLE symphonie.tf_samples_dico_intern_element_dicie OWNER TO :proprietaire;
	
	-- TABLE : tf_samples_dico_intern_type_dicit
	CREATE FOREIGN TABLE symphonie.tf_samples_dico_intern_type_dicit (
		data_type_id			INTEGER						NOT NULL,
		data_type_name			CHARACTER VARYING(100)		NOT NULL,
		data_type_fid			INTEGER						NOT NULL,
		data_type_comment		CHARACTER VARYING(1000)
	)SERVER server_samples OPTIONS (schema_name 'data', table_name 'dico_interne_type');
	ALTER TABLE symphonie.tf_samples_dico_intern_type_dicit OWNER TO :proprietaire;
	
-- ********************************************************
-- ***** création des vues des FOREIGN TABLE de SAMPLES ***
-- ********************************************************

	-- vue : vf_samples_vsam
	CREATE VIEW symphonie.vf_samples_vsam AS
		SELECT * 
		FROM symphonie.tf_samples_samples_sam;
	
	ALTER VIEW symphonie.vf_samples_vsam OWNER TO :proprietaire;
	
	-- vue : vf_barcode_vbarc
	CREATE VIEW symphonie.vf_barcode_vbarc AS
		SELECT * 
		FROM symphonie.tf_samples_barcode_barc;
		
	ALTER VIEW symphonie.vf_barcode_vbarc OWNER TO :proprietaire;
		
	-- vue : vf_operator_vope
	CREATE VIEW symphonie.vf_operator_vope AS
		SELECT * 
		FROM symphonie.tf_samples_operator_ope;
	
	ALTER VIEW symphonie.vf_operator_vope OWNER TO :proprietaire;
	
	-- vue : vf_project_vproj
	CREATE VIEW symphonie.vf_project_vproj AS
		SELECT * 
		FROM symphonie.tf_samples_project_proj;
	
	ALTER VIEW symphonie.vf_project_vproj OWNER TO :proprietaire;
	
	-- vue : vf_terrain_vter
	CREATE VIEW symphonie.vf_terrain_vter AS
		SELECT * 
		FROM symphonie.tf_samples_terrain_paper_tpap;
	
	ALTER VIEW symphonie.vf_terrain_vter OWNER TO :proprietaire;
		
	-- vue : vf_dico_interne_element_vdicie
	CREATE VIEW symphonie.vf_dico_interne_element_vdicie AS
		SELECT * 
		FROM symphonie.tf_samples_dico_intern_element_dicie;
	
	ALTER VIEW symphonie.vf_dico_interne_element_vdicie OWNER TO :proprietaire;
		
	-- vue : vf_dico_interne_type_vdicit
	CREATE VIEW symphonie.vf_dico_interne_type_vdicit AS
		SELECT * 
		FROM symphonie.tf_samples_dico_intern_type_dicit;
	
	ALTER VIEW symphonie.vf_dico_interne_type_vdicit OWNER TO :proprietaire;

-- *************************************************
-- ***** création de la TABLE de configuration *****
-- *********************************************-***

	--table : t_formulaire_access_configuration_faconf
	CREATE TABLE symphonie.t_formulaire_access_configuration_faconf (
		faconf_id               SERIAL			NOT NULL,
		faconf_sgbd             VARCHAR(100)	NOT NULL,
		faconf_version			VARCHAR(4)		NULL,
		faconf_odbc				BOOLEAN         NOT NULL,
		faconf_deconnecte		BOOLEAN         NOT NULL,
		faconf_num_instance		INTEGER			NULL,
		faconf_database			VARCHAR(100)	NOT NULL,
		faconf_role_ecriture	VARCHAR(100)	NULL,
		faconf_tag				VARCHAR(255)	NULL,
		CONSTRAINT t_formulaire_access_configuration_faconf_pkey PRIMARY KEY (faconf_id)
	);

	COMMENT ON TABLE symphonie.t_formulaire_access_configuration_faconf IS
	'Table contenant une partie de la configuration de l''application access.';
	COMMENT ON COLUMN symphonie.t_formulaire_access_configuration_faconf.faconf_id IS
	'Identifiant de la configuration.';
	COMMENT ON COLUMN symphonie.t_formulaire_access_configuration_faconf.faconf_sgbd IS
	'Nom du sgbd utilisé.';
	COMMENT ON COLUMN symphonie.t_formulaire_access_configuration_faconf.faconf_version IS
	'Version du sgbd utilisé.';
	COMMENT ON COLUMN symphonie.t_formulaire_access_configuration_faconf.faconf_odbc IS
	'Booléan indiquant si l''application est connectée à la base en mode connecté (tables liées).';
	COMMENT ON COLUMN symphonie.t_formulaire_access_configuration_faconf.faconf_deconnecte IS
	'Booléan indiquant si l''application est connectée à la base en mode déconnecté.';
	COMMENT ON COLUMN symphonie.t_formulaire_access_configuration_faconf.faconf_num_instance IS
	'Chiffre indiquant le numéro de l''instance, information utilisée en mode déconnecté pour la saisie à plusieurs.';
	COMMENT ON COLUMN symphonie.t_formulaire_access_configuration_faconf.faconf_database IS
	'Nom de la base de donnée utilsée.';
	COMMENT ON COLUMN symphonie.t_formulaire_access_configuration_faconf.faconf_role_ecriture IS
	'Nom du rôle ayant les droits en écriture.';
	COMMENT ON COLUMN symphonie.t_formulaire_access_configuration_faconf.faconf_tag IS
	'Tag permettant d''effectuer un traitement particulier selon la valeur de ce dernier.';
	
	ALTER TABLE symphonie.t_formulaire_access_configuration_faconf OWNER TO :proprietaire;
	
	--insertion
	INSERT INTO symphonie.t_formulaire_access_configuration_faconf(faconf_sgbd, faconf_version, faconf_odbc, faconf_deconnecte,faconf_num_instance, faconf_database, faconf_role_ecriture,faconf_tag)
	VALUES ('Postgresql', '9.5', true, false, 1, 'db_symphonie', 'symphonie_ecriture', null);

	
-- ********************************************
-- ***** création des tables de référence *****
-- ********************************************

	-- TABLE : pays
	CREATE TABLE symphonie.tr_pays_pays (
	   pays_id         SERIAL               NOT NULL,
	   pays_code       VARCHAR(3)           NOT NULL,
	   pays_nom        VARCHAR(100)			NOT NULL,
	   CONSTRAINT c_uni_pays_code UNIQUE (pays_code),
	   CONSTRAINT tr_pays_pays_pkey PRIMARY KEY (pays_id)
	);
	COMMENT ON TABLE symphonie.tr_pays_pays IS
	'Table contenant tous les pays (source : http://www.insee.fr/fr/methodes/nomenclatures/cog/telechargement.asp?annee=2016)';
	COMMENT ON COLUMN symphonie.tr_pays_pays.pays_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.tr_pays_pays.pays_code IS
	'Code du pays.';
	COMMENT ON COLUMN symphonie.tr_pays_pays.pays_nom IS
	'Nom du pays.';
	ALTER TABLE symphonie.tr_pays_pays OWNER TO :proprietaire;

	-- TABLE : regions
	CREATE TABLE symphonie.tr_regions_reg (
		reg_id         SERIAL			NOT NULL,
		reg_pays_id    INT4				NOT NULL,
		reg_code       INT4				NOT NULL,
		reg_nom        VARCHAR(30)		NOT NULL,
		CONSTRAINT c_uni_regions_code UNIQUE (reg_code),
		CONSTRAINT tr_regions_reg_pkey PRIMARY KEY (reg_id),
		CONSTRAINT c_fk_pays_reg FOREIGN KEY (reg_pays_id)
			REFERENCES symphonie.tr_pays_pays(pays_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tr_regions_reg IS
	'Table contenant toutes les régions françaises (source : http://www.insee.fr/fr/methodes/nomenclatures/cog/telechargement.asp?annee=2016)';
	COMMENT ON COLUMN symphonie.tr_regions_reg.reg_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.tr_regions_reg.reg_pays_id IS
	'Pays dans lequel se situe la commune.';
	COMMENT ON COLUMN symphonie.tr_regions_reg.reg_code IS
	'Code de la régions.';
	COMMENT ON COLUMN symphonie.tr_regions_reg.reg_nom IS
	'Nom de la région.';
	COMMENT ON CONSTRAINT c_fk_pays_reg ON symphonie.tr_regions_reg IS
	'Rattachement d''un pays à uns et uns seuls région.';
	ALTER TABLE symphonie.tr_regions_reg OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_reg_pays_id_reg
	CREATE INDEX x_btr_reg_pays_id_reg ON symphonie.tr_regions_reg USING BTREE (reg_pays_id);
	COMMENT ON INDEX symphonie.x_btr_reg_pays_id_reg IS 
	'Index sur le rattachement à un pays afin d''optimiser le temps des requètes';
	
	-- TABLE : departement
	CREATE TABLE symphonie.tr_departements_dep (
		dep_id			SERIAL			NOT NULL,
		dep_reg_id		INT4			NOT NULL,
		dep_code			VARCHAR(3)		NOT NULL,
		dep_nom			VARCHAR(30)		NOT NULL,
		CONSTRAINT c_uni_dep_code UNIQUE (dep_code),
		CONSTRAINT tr_departements_dep_pkey PRIMARY KEY (dep_id),
		CONSTRAINT c_fk_reg_dep FOREIGN KEY (dep_reg_id)
			REFERENCES symphonie.tr_regions_reg(reg_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tr_departements_dep IS
	'Table contenant tous les départements français (source : http://www.insee.fr/fr/methodes/nomenclatures/cog/telechargement.asp?annee=2016)';
	COMMENT ON COLUMN symphonie.tr_departements_dep.dep_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.tr_departements_dep.dep_reg_id IS
	'Région dans laquelle se situe le département.';
	COMMENT ON COLUMN symphonie.tr_departements_dep.dep_code IS
	'Code du département.';
	COMMENT ON COLUMN symphonie.tr_departements_dep.dep_nom IS
	'Nom du département.';
	COMMENT ON CONSTRAINT c_fk_reg_dep ON symphonie.tr_departements_dep IS
	'Rattachement d''une région à un et un seul département.';
	ALTER TABLE symphonie.tr_departements_dep OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_dep_reg_id_dep
	CREATE INDEX x_btr_dep_reg_id_dep ON symphonie.tr_departements_dep USING BTREE (dep_reg_id);
	COMMENT ON INDEX symphonie.x_btr_dep_reg_id_dep IS 
	'Index sur le rattachement à une région afin d''optimiser le temps des requètes';

	-- TABLE : communes
	CREATE TABLE symphonie.tr_communes_com (
		com_id                  SERIAL               NOT NULL,
		com_dep_id         	   INT4                 NOT NULL,
		com_insee_code          VARCHAR(6)	        NOT NULL,
		com_nom                 VARCHAR(50)          NOT NULL,
		CONSTRAINT c_uni_communes_insee_code UNIQUE (com_insee_code),
		CONSTRAINT commune_com_pkey PRIMARY KEY (com_id),
		CONSTRAINT c_fk_dep_com FOREIGN KEY (com_dep_id)
			REFERENCES symphonie.tr_departements_dep(dep_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE

	);
	COMMENT ON TABLE symphonie.tr_communes_com IS
	'Table de référence contenant toutes les communes françaises (source : http://www.insee.fr/fr/methodes/nomenclatures/cog/telechargement.asp?annee=2016)';
	COMMENT ON COLUMN symphonie.tr_communes_com.com_id IS
	'DépArtement dans lequel se situe la commune.';
	COMMENT ON COLUMN symphonie.tr_communes_com.com_dep_id IS
	'Département dans lequel se situe la commune.';
	COMMENT ON COLUMN symphonie.tr_communes_com.com_insee_code IS
	'Code insee de la commune.';
	COMMENT ON COLUMN symphonie.tr_communes_com.com_nom IS
	'Nom de la commune.';
	COMMENT ON CONSTRAINT c_fk_dep_com ON symphonie.tr_communes_com IS
	'Rattachement d''une commune à une et une seule région.';
	ALTER TABLE symphonie.tr_communes_com OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_com_dep_id_com
	CREATE INDEX x_btr_com_dep_id_com ON symphonie.tr_communes_com USING BTREE (com_dep_id);
	COMMENT ON INDEX symphonie.x_btr_dep_reg_id_dep IS 
	'Index sur le rattachement à un département afin d''optimiser le temps des requètes';

	-- TABLE : referentiel_geographique
	CREATE TABLE symphonie.tr_referentiel_geographique_refg (
		refg_id				SERIAL			NOT NULL,
		refg_libelle		VARCHAR(100)	NOT NULL,
		refg_epsg			INT4			NOT NULL,
		CONSTRAINT c_uni_refg_libelle UNIQUE (refg_libelle),
		CONSTRAINT c_uni_refg_epsg UNIQUE (refg_epsg),
		CONSTRAINT tr_referentiel_geographique_refg_pkey PRIMARY KEY (refg_id)
	);
	COMMENT ON TABLE symphonie.tr_referentiel_geographique_refg IS
	'Tables contenant les référentiels géographiques.';
	COMMENT ON COLUMN symphonie.tr_referentiel_geographique_refg.refg_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.tr_referentiel_geographique_refg.refg_libelle IS
	'Libellé du référentiel géographique.';
	COMMENT ON COLUMN symphonie.tr_referentiel_geographique_refg.refg_epsg IS
	'Code epsg (unique) qui dépend du référentiel géographique.';
	ALTER TABLE symphonie.tr_referentiel_geographique_refg OWNER TO :proprietaire;

	-- TABLE : unite
	CREATE TABLE symphonie.tr_unite_uni (
		uni_id              SERIAL			NOT NULL,
		uni_symbole         VARCHAR(5)		NOT NULL,
		uni_nom				VARCHAR(50)		NOT NULL,
		uni_description		VARCHAR(255)	NULL,
		uni_type_donnee		VARCHAR(25)		NULL,
		CONSTRAINT c_uni_uni_symbole UNIQUE (uni_symbole),
		CONSTRAINT c_uni_uni_nom UNIQUE (uni_nom),
		CONSTRAINT tr_unite_uni_pkey PRIMARY KEY (uni_id)
	);
	COMMENT ON TABLE symphonie.tr_unite_uni IS
	'Table contenant les unités de mesure.';
	COMMENT ON COLUMN symphonie.tr_unite_uni.uni_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.tr_unite_uni.uni_symbole IS
	'Symbole de l''unité (ex : cm).';
	COMMENT ON COLUMN symphonie.tr_unite_uni.uni_nom IS
	'Nom de l''unité (ex : centimètre)';
	COMMENT ON COLUMN symphonie.tr_unite_uni.uni_description IS
	'Description de l''unité.';
	COMMENT ON COLUMN symphonie.tr_unite_uni.uni_type_donnee IS
	'Type de donnée de l''unité (ex : entier, alphanumérique).';
	ALTER TABLE symphonie.tr_unite_uni OWNER TO :proprietaire;

	-- TABLE : genre
	CREATE TABLE symphonie.tr_genre_genr (
		genr_id              SERIAL               NOT NULL,
		genr_code	         VARCHAR(32)          NOT NULL,
		genr_nom	         VARCHAR(255)         NOT NULL,
		CONSTRAINT c_uni_genr_code UNIQUE (genr_code),
		CONSTRAINT tr_genre_genr_pkey PRIMARY KEY (genr_id)
	);
	COMMENT ON TABLE symphonie.tr_genre_genr IS
	'Table contenant tous les libellés des semis.';
	COMMENT ON COLUMN symphonie.tr_genre_genr.genr_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.tr_genre_genr.genr_code IS
	'Code du genre.';
	COMMENT ON COLUMN symphonie.tr_genre_genr.genr_nom IS
	'Nom du genre.';
	ALTER TABLE symphonie.tr_genre_genr OWNER TO :proprietaire;
	
	-- TABLE : espece
	CREATE TABLE symphonie.tr_espece_esp (
		esp_id					SERIAL			NOT NULL,
		esp_genr_id				INT4			NOT NULL,
		esp_nom_scientifique		VARCHAR(100)	NOT NULL,
		esp_nom					VARCHAR(100)	NOT NULL,
		esp_rang					VARCHAR(25)		NOT NULL,
		CONSTRAINT c_uni_esp_nom_scientifique UNIQUE (esp_nom_scientifique),
		CONSTRAINT tr_espece_esp_pkey PRIMARY KEY (esp_id),
		CONSTRAINT c_fk_genr_esp FOREIGN KEY (esp_genr_id)
			REFERENCES symphonie.tr_genre_genr(genr_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	
	COMMENT ON TABLE symphonie.tr_espece_esp IS
	'Table contenant toutes les espèces.';
	COMMENT ON COLUMN symphonie.tr_espece_esp.esp_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.tr_espece_esp.esp_genr_id IS
	'Genre de l''espèce.';
	COMMENT ON COLUMN symphonie.tr_espece_esp.esp_nom_scientifique IS
	'Nom scientifique de l''espèce.';
	COMMENT ON COLUMN symphonie.tr_espece_esp.esp_nom IS
	'Nom de l''espèce.';
	COMMENT ON COLUMN symphonie.tr_espece_esp.esp_rang IS
	'Rang de l''espèce.';
	COMMENT ON CONSTRAINT c_fk_genr_esp ON symphonie.tr_espece_esp IS
	'Rattachement d''une commune à une et une seule région.';
	ALTER TABLE symphonie.tr_espece_esp OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_esp_genr_id_esp
	CREATE INDEX x_btr_esp_genr_id_esp ON symphonie.tr_espece_esp USING BTREE (esp_genr_id);
	COMMENT ON INDEX symphonie.x_btr_esp_genr_id_esp IS
	'Index sur le rattachement à un genre afin d''optimiser le temps des requètes';

	-- TABLE : methode
	CREATE TABLE symphonie.tr_methode_met (
		met_id				SERIAL			NOT NULL,
		met_code			VARCHAR(15)		NOT NULL,
		met_nom				VARCHAR(100)	NOT NULL,
		met_description		TEXT			NULL,
		CONSTRAINT c_uni_met_code UNIQUE (met_code),
		CONSTRAINT tr_methode_met_pkey PRIMARY KEY (met_id)
	);
	COMMENT ON TABLE symphonie.tr_methode_met IS
	'Table contenant toutes les méthodes utilisées lors des mesures.';
	COMMENT ON COLUMN symphonie.tr_methode_met.met_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.tr_methode_met.met_code IS
	'Code de la méthode.';
	COMMENT ON COLUMN symphonie.tr_methode_met.met_nom IS
	'Nom de la méthode.';
	COMMENT ON COLUMN symphonie.tr_methode_met.met_description IS
	'Description de la méthode.';
	ALTER TABLE symphonie.tr_methode_met OWNER TO :proprietaire;

	-- TABLE : norme
	CREATE TABLE symphonie.tr_norme_norm (
		norm_id				SERIAL			NOT NULL,
		norm_nom			VARCHAR(50)		NOT NULL,
		norm_date			DATE			NOT NULL,
		norm_url			VARCHAR(255)	NULL,
		norm_version		VARCHAR(5)		NULL,
		CONSTRAINT c_uni_norm_url UNIQUE (norm_url),
		CONSTRAINT c_uni_norm_nom_version UNIQUE (norm_nom, norm_version),
		CONSTRAINT tr_norme_norm_pkey PRIMARY KEY (norm_id)
	);
	COMMENT ON TABLE symphonie.tr_norme_norm IS
	'Table contenant toutes les normes.';
	COMMENT ON COLUMN symphonie.tr_norme_norm.norm_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.tr_norme_norm.norm_nom IS
	'Nom de la norme.';
	COMMENT ON COLUMN symphonie.tr_norme_norm.norm_date IS
	'Date de parution de la norme.';
	COMMENT ON COLUMN symphonie.tr_norme_norm.norm_url IS
	'Chemin d''accés à un fichier joint concernant la norme.';
	COMMENT ON COLUMN symphonie.tr_norme_norm.norm_version IS
	'Version de la norme.';
	ALTER TABLE symphonie.tr_norme_norm OWNER TO :proprietaire;
	
	-- TABLE : ontologie
	CREATE TABLE symphonie.tr_ontologie_ont (
		ont_id				SERIAL				NOT NULL,
		ont_code			VARCHAR(10)			NOT NULL,
		ont_nom				VARCHAR(100)        NOT NULL,
		ont_description		VARCHAR(255)		NULL,
		CONSTRAINT c_uni_ont_code UNIQUE (ont_code),
		CONSTRAINT tr_ontologie_ont_pkey PRIMARY KEY (ont_id)
	);
	COMMENT ON TABLE symphonie.tr_ontologie_ont IS
	'Table contenant toutes les variables ISsuent de l''ontologie.';
	COMMENT ON COLUMN symphonie.tr_ontologie_ont.ont_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.tr_ontologie_ont.ont_code IS
	'Code de la variable ISsue de l''ontologie.';
	COMMENT ON COLUMN symphonie.tr_ontologie_ont.ont_nom IS
	'Nom de la variable ISsue de l''ontologie.';
	COMMENT ON COLUMN symphonie.tr_ontologie_ont.ont_description IS
	'Description de la variable ISsue de l''ontologie.';
	ALTER TABLE symphonie.tr_ontologie_ont OWNER TO :proprietaire;

	
-- ******************************************
-- ***** création des tables de données *****
-- ******************************************

-- TABLE : role
	CREATE TABLE symphonie.t_role_rol (
		rol_id			SERIAL			NOT NULL,
		rol_libelle		VARCHAR(100)	NOT NULL,
		CONSTRAINT c_uni_rol_libelle UNIQUE (rol_libelle),
		CONSTRAINT t_role_rol_pkey PRIMARY KEY (rol_id)
	);
	COMMENT ON TABLE symphonie.t_role_rol IS
	'Table contenant les différents roles pouvant être affectés à des opérateurs.';
	COMMENT ON COLUMN symphonie.t_role_rol.rol_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_role_rol.rol_libelle IS
	'Libellé du role.';
	ALTER TABLE symphonie.t_role_rol OWNER TO :proprietaire;

	-- TABLE : type de materiel
	CREATE TABLE symphonie.t_type_materiel_mat (
		mat_id              SERIAL               NOT NULL,
		mat_libelle         VARCHAR(32)          NOT NULL,
		CONSTRAINT c_uni_mat_libelle UNIQUE (mat_libelle),
		CONSTRAINT t_type_materiel_mat_pkey PRIMARY KEY (mat_id)
	);
	COMMENT ON TABLE symphonie.t_type_materiel_mat IS
	'Table contenant tous les types de materiaux végétaux.';
	COMMENT ON COLUMN symphonie.t_type_materiel_mat.mat_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_type_materiel_mat.mat_libelle IS
	'Libellé du maeriel végétal.';
	ALTER TABLE symphonie.t_type_materiel_mat OWNER TO :proprietaire;
	
	-- TABLE : position balise
	CREATE TABLE symphonie.t_position_balise_posb (
		posb_id              SERIAL               NOT NULL,
		posb_libelle         VARCHAR(32)          NOT NULL,
		CONSTRAINT c_uni_posb_libelle UNIQUE (posb_libelle),
		CONSTRAINT t_position_balise_posb_pkey PRIMARY KEY (posb_id)
	);
	COMMENT ON TABLE symphonie.t_position_balise_posb IS
	'Table contenant toutes les positions de balises possibles.';
	COMMENT ON COLUMN symphonie.t_position_balise_posb.posb_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_position_balise_posb.posb_libelle IS
	'Libellé de la position de la balise.';
	ALTER TABLE symphonie.t_position_balise_posb OWNER TO :proprietaire;
	
	-- TABLE : exposition
	CREATE TABLE symphonie.t_exposition_expo (
		expo_id              SERIAL               NOT NULL,
		expo_libelle         VARCHAR(50)          NOT NULL,
		CONSTRAINT c_uni_expo_libelle UNIQUE (expo_libelle),
		CONSTRAINT t_exposition_expo_pkey PRIMARY KEY (expo_id)
	);
	COMMENT ON TABLE symphonie.t_exposition_expo IS
	'Table contenant les expositions.';
	COMMENT ON COLUMN symphonie.t_exposition_expo.expo_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_exposition_expo.expo_libelle IS
	'Libellé de l''exposition.';
	ALTER TABLE symphonie.t_exposition_expo OWNER TO :proprietaire;
	
	-- TABLE : type de propriete
	CREATE TABLE symphonie.t_type_propriete_typp (
		typp_id              SERIAL               NOT NULL,
		typp_libelle         VARCHAR(32)          NOT NULL,
		CONSTRAINT c_uni_typp_libelle UNIQUE (typp_libelle),
		CONSTRAINT t_type_propriete_typp_pkey PRIMARY KEY (typp_id)
	);
	COMMENT ON TABLE symphonie.t_type_propriete_typp IS
	'Table contenant tous les libellés concernant les différents types de propriétés.';
	COMMENT ON COLUMN symphonie.t_type_propriete_typp.typp_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_type_propriete_typp.typp_libelle IS
	'Libellé du type de propriété.';
	ALTER TABLE symphonie.t_type_propriete_typp OWNER TO :proprietaire;
	
	-- TABLE : activite
	CREATE TABLE symphonie.t_activite_acti (
		acti_id              SERIAL               NOT NULL,
		acti_libelle         VARCHAR(25)          NOT NULL,
		CONSTRAINT c_uni_acti_libelle UNIQUE (acti_libelle),
		CONSTRAINT t_activite_acti_pkey PRIMARY KEY (acti_id)
	);
	COMMENT ON TABLE symphonie.t_activite_acti IS
	'Table contenant les libellés des différents niveaux d''activités attribué à un dispositif.';
	COMMENT ON COLUMN symphonie.t_activite_acti.acti_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_activite_acti.acti_libelle IS
	'Libellé du niveau d''activité.';
	ALTER TABLE symphonie.t_activite_acti OWNER TO :proprietaire;
	
	-- TABLE : orientation des lignes
	CREATE TABLE symphonie.t_orientation_ligne_oril (
		oril_id              SERIAL               NOT NULL,
		oril_libelle         VARCHAR(32)          NOT NULL,
		CONSTRAINT c_uni_oril_libelle UNIQUE (oril_libelle),
		CONSTRAINT t_orientation_ligne_oril_pkey PRIMARY KEY (oril_id)
	);
	COMMENT ON TABLE symphonie.t_orientation_ligne_oril IS
	'Table contenant tous les libellés concernant les orientations de ligne possibles.';
	COMMENT ON COLUMN symphonie.t_orientation_ligne_oril.oril_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_orientation_ligne_oril.oril_libelle IS
	'Libellé de l''oriantation des lignes.';
	ALTER TABLE symphonie.t_orientation_ligne_oril OWNER TO :proprietaire;
	
	-- TABLE : regroupement de dispositif
	CREATE TABLE symphonie.t_regroupement_dispositif_rgpd (
		rgpd_id			SERIAL			NOT NULL,
		rgpd_nom		VARCHAR(255)	NOT NULL,
		CONSTRAINT c_uni_rgpd_nom UNIQUE (rgpd_nom),
		CONSTRAINT t_regroupement_dispositif_rgpd_pkey PRIMARY KEY (rgpd_id)
	);
	COMMENT ON TABLE symphonie.t_regroupement_dispositif_rgpd IS
	'Table contenant tous les regroupement de dispositifs.';
	COMMENT ON COLUMN symphonie.t_regroupement_dispositif_rgpd.rgpd_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_regroupement_dispositif_rgpd.rgpd_nom IS
	'Nom du regroupement de dispositifs.';
	ALTER TABLE symphonie.t_regroupement_dispositif_rgpd OWNER TO :proprietaire;
	
	-- TABLE : drainage
	CREATE TABLE symphonie.t_drainage_drai (
		drai_id					SERIAL				NOT NULL,
		drai_nom				VARCHAR(50)			NOT NULL,
		drai_commentaire		VARCHAR(255)		NULL,
		CONSTRAINT c_uni_drai_nom UNIQUE (drai_nom),
		CONSTRAINT t_drainage_drai_pkey PRIMARY KEY (drai_id)
	);
	COMMENT ON TABLE symphonie.t_drainage_drai IS
	'Table contenant tous les drainages.';
	COMMENT ON COLUMN symphonie.t_drainage_drai.drai_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_drainage_drai.drai_nom IS
	'Nom du drainage.';
	COMMENT ON COLUMN symphonie.t_drainage_drai.drai_commentaire IS
	'Commentaire concernant le drainage.';
	ALTER TABLE symphonie.t_drainage_drai OWNER TO :proprietaire;
	
	
	-- TABLE : sylvo eco-region
	CREATE TABLE symphonie.t_sylvo_ecoregion_sylv (
		sylv_id				SERIAL			NOT NULL,
		sylv_nom			VARCHAR(100)	NOT NULL,
		sylv_description	VARCHAR(255)	NULL,
		CONSTRAINT c_uni_sylv_nom UNIQUE (sylv_nom),
		CONSTRAINT t_sylvo_ecoregion_sylv_pkey PRIMARY KEY (sylv_id)
	);
	COMMENT ON TABLE symphonie.t_sylvo_ecoregion_sylv IS
	'Table contenant toutes les sylvo-écorégions françaises.';
	COMMENT ON COLUMN symphonie.t_sylvo_ecoregion_sylv.sylv_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_sylvo_ecoregion_sylv.sylv_nom IS
	'Nom de la sylvo-écorégions.';
	COMMENT ON COLUMN symphonie.t_sylvo_ecoregion_sylv.sylv_description IS
	'Description de la sylvo-écorégion.';
	ALTER TABLE symphonie.t_sylvo_ecoregion_sylv OWNER TO :proprietaire;
	
	-- TABLE : type de dispositif
	CREATE TABLE symphonie.t_type_dispositif_typd (
		typd_id				SERIAL			NOT NULL,
		typd_code			INT4			NOT NULL,
		typd_nom_court		VARCHAR(50)		NOT NULL,
		typd_nom			VARCHAR(255)	NOT NULL,
		typd_commentaire	TEXT			NULL,
		CONSTRAINT c_uni_typd_code UNIQUE (typd_code),
		CONSTRAINT t_type_dispositif_typd_pkey PRIMARY KEY (typd_id)
	);
	COMMENT ON TABLE symphonie.t_type_dispositif_typd IS
	'Table contenant tous les libellés des différents type de dispositif.';
	COMMENT ON COLUMN symphonie.t_type_dispositif_typd.typd_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_type_dispositif_typd.typd_code IS
	'Code du type de dispositif.';
	COMMENT ON COLUMN symphonie.t_type_dispositif_typd.typd_nom_court IS
	'Nom court du type de dispositif.';
	COMMENT ON COLUMN symphonie.t_type_dispositif_typd.typd_nom IS
	'Nom du type de dispositif.';
	COMMENT ON COLUMN symphonie.t_type_dispositif_typd.typd_commentaire IS
	'Commentaire concernant le type de dispositif.';
	ALTER TABLE symphonie.t_type_dispositif_typd OWNER TO :proprietaire;
	
	-- TABLE : semis
	CREATE TABLE symphonie.t_semis_sem (
		sem_id              SERIAL				NOT NULL,
		sem_libelle         VARCHAR(32)			NOT NULL,
		sem_commentaire		VARCHAR(255)		NULL,
		CONSTRAINT c_uni_sem_libelle UNIQUE (sem_libelle),
		CONSTRAINT t_semis_sem_pkey PRIMARY KEY (sem_id)
	);
	COMMENT ON TABLE symphonie.t_semis_sem IS
	'Table contenant tous les libellés des semis.';
	COMMENT ON COLUMN symphonie.t_semis_sem.sem_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_semis_sem.sem_libelle IS
	'Libellé du semis.';
	COMMENT ON COLUMN symphonie.t_semis_sem.sem_commentaire IS
	'Commentaire concernant le semis.';
	ALTER TABLE symphonie.t_semis_sem OWNER TO :proprietaire;
	
	-- TABLE : mode de plantation
	CREATE TABLE symphonie.t_mode_plantation_modp (
		modp_id				SERIAL			NOT NULL,
		modp_libelle		VARCHAR(25)		NOT NULL,
		modp_commentaire	VARCHAR(255)	NULL,
		CONSTRAINT c_uni_modp_libelle UNIQUE (modp_libelle),
		CONSTRAINT t_mode_plantation_modp_pkey PRIMARY KEY (modp_id)
	);
	COMMENT ON TABLE symphonie.t_mode_plantation_modp IS
	'Tables contenant tous les libellés des modes de plantation.';
	COMMENT ON COLUMN symphonie.t_mode_plantation_modp.modp_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_mode_plantation_modp.modp_libelle IS
	'Libellé du mode de plantation.';
	COMMENT ON COLUMN symphonie.t_mode_plantation_modp.modp_commentaire IS
	'Commentaire concernant le mode de plantation.';
	ALTER TABLE symphonie.t_mode_plantation_modp OWNER TO :proprietaire;
	
	-- TABLE : preparation des racines
	CREATE TABLE symphonie.t_preparation_racine_prepr (
		prepr_id				SERIAL				NOT NULL,
		prepr_libelle			VARCHAR(25)			NOT NULL,
		prepr_commentaire		VARCHAR(255)		NULL,
		CONSTRAINT c_uni_prepr_libelle UNIQUE (prepr_libelle),
		CONSTRAINT t_preparation_racine_prepr_pkey PRIMARY KEY (prepr_id)
	);
	COMMENT ON TABLE symphonie.t_preparation_racine_prepr IS
	'Table contenant tous les libellés concernant la façon dont sont préparées les racines.';
	COMMENT ON COLUMN symphonie.t_preparation_racine_prepr.prepr_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_preparation_racine_prepr.prepr_libelle IS
	'Libellé de la méthode utilisée pour préparer les racines.';
	COMMENT ON COLUMN symphonie.t_preparation_racine_prepr.prepr_commentaire IS
	'Commentaire concernant la méthode utilisée pour préparer les racines.';
	ALTER TABLE symphonie.t_preparation_racine_prepr OWNER TO :proprietaire;
	
	-- TABLE : antecedent
	CREATE TABLE symphonie.t_antecedent_ant (
		ant_id              SERIAL			NOT NULL,
		ant_libelle         VARCHAR(32)		NOT NULL,
		ant_description		VARCHAR(255)	NULL,
		CONSTRAINT c_uni_ant_libelle UNIQUE (ant_libelle),
		CONSTRAINT t_antecedent_ant_pkey PRIMARY KEY (ant_id)
	);
	COMMENT ON TABLE symphonie.t_antecedent_ant IS
	'Table contenant tous les antécédents concernant les dispositifs.';
	COMMENT ON COLUMN symphonie.t_antecedent_ant.ant_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_antecedent_ant.ant_libelle IS
	'Libellé de l''antécedent.';
	COMMENT ON COLUMN symphonie.t_antecedent_ant.ant_description IS
	'Description de l''antécédent.';
	ALTER TABLE symphonie.t_antecedent_ant OWNER TO :proprietaire;
	
	-- TABLE : description ecologique
	CREATE TABLE symphonie.t_description_ecologique_ecod (
		ecod_id	             SERIAL               NOT NULL,
		ecod_expo_id         INT4                 NULL,
		ecod_flore_herbace   VARCHAR(255)         NULL,
		ecod_flore_arbustive VARCHAR(255)         NULL,
		ecod_roche_mere      VARCHAR(100)         NULL,
		ecod_sol_pedo        VARCHAR(100)         NULL,
		ecod_ph              DECIMAL(2,1)         NULL,
		ecod_profondeur      INT4                 NULL,
		ecod_texture         VARCHAR(50)          NULL,
		ecod_abondance_eg    INT2                 NULL,
		ecod_commentaire     VARCHAR(255)         NULL,
		CONSTRAINT t_description_ecologique_ecod_pkey PRIMARY KEY (ecod_id),
		CONSTRAINT c_fk_expo_ecod FOREIGN KEY (ecod_expo_id)
			REFERENCES symphonie.t_exposition_expo(expo_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.t_description_ecologique_ecod IS
	'Table contenant toutes les descriptions écologiques.';
	COMMENT ON COLUMN symphonie.t_description_ecologique_ecod.ecod_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_description_ecologique_ecod.ecod_expo_id IS
	'Exposition associée à la description écologique.';
	COMMENT ON COLUMN symphonie.t_description_ecologique_ecod.ecod_flore_herbace IS
	'Flore herbacée présente dans le dispositif.';
	COMMENT ON COLUMN symphonie.t_description_ecologique_ecod.ecod_flore_arbustive IS
	'Flore arbustive présente dans le dispositif.';
	COMMENT ON COLUMN symphonie.t_description_ecologique_ecod.ecod_roche_mere IS
	'Roche mère principale du dispositif.';
	COMMENT ON COLUMN symphonie.t_description_ecologique_ecod.ecod_sol_pedo IS
	'Sol pédologique du dispositif.';
	COMMENT ON COLUMN symphonie.t_description_ecologique_ecod.ecod_ph IS
	'';
	COMMENT ON COLUMN symphonie.t_description_ecologique_ecod.ecod_profondeur IS
	'';
	COMMENT ON COLUMN symphonie.t_description_ecologique_ecod.ecod_texture IS
	'';
	COMMENT ON COLUMN symphonie.t_description_ecologique_ecod.ecod_abondance_eg IS
	'';
	COMMENT ON COLUMN symphonie.t_description_ecologique_ecod.ecod_commentaire IS
	'';
	COMMENT ON CONSTRAINT c_fk_expo_ecod ON symphonie.t_description_ecologique_ecod IS
	'Rattachement d''une exposition à une et une seule description écologique.';
	ALTER TABLE symphonie.t_description_ecologique_ecod OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_ecod_expo_id_ecod
	CREATE INDEX x_btr_ecod_expo_id_ecod ON symphonie.t_description_ecologique_ecod USING BTREE (ecod_expo_id);
	COMMENT ON INDEX symphonie.x_btr_ecod_expo_id_ecod IS 
	'Index sur le rattachement à une exposition afin d''optimiser le temps des requètes';
	
	-- TABLE : operateur
	CREATE TABLE symphonie.t_operateur_ope (
		ope_id              SERIAL				NOT NULL,
		ope_nom             VARCHAR(32)			NOT NULL,
		ope_prenom          VARCHAR(24)			NOT NULL,
		ope_tel             VARCHAR(15)			NULL,
		ope_mail            VARCHAR(100)		NULL,
		ope_login			VARCHAR(100)		NULL,
		ope_compte_noyau	BOOLEAN				null 		DEFAULT NULL,
		ope_login_noyau		VARCHAR(255)		NULL,
		CONSTRAINT c_uni_ope_mail UNIQUE (ope_mail),
		CONSTRAINT t_operateur_ope_pkey PRIMARY KEY (ope_id)
	);
	COMMENT ON TABLE symphonie.t_operateur_ope IS
	'Table contenant tous les opérateurs.';
	COMMENT ON COLUMN symphonie.t_operateur_ope.ope_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_operateur_ope.ope_nom IS
	'Nom de l''opérateur.';
	COMMENT ON COLUMN symphonie.t_operateur_ope.ope_prenom IS
	'Prénom de l''opérateur.';
	COMMENT ON COLUMN symphonie.t_operateur_ope.ope_tel IS
	'Numéro de téléphone de l''opérateur.';
	COMMENT ON COLUMN symphonie.t_operateur_ope.ope_mail IS
	'Adresse mail de l''opérateur.';
	COMMENT ON COLUMN symphonie.t_operateur_ope.ope_login IS
	'Login permettant à l''opérateur de se connecter a Symphonie.';
	COMMENT ON COLUMN symphonie.t_operateur_ope.ope_compte_noyau IS
	'Boolean indiquent si l''opérateur possède un compte sur le noyau Orléanais.';
	COMMENT ON COLUMN symphonie.t_operateur_ope.ope_login_noyau IS
	'Login du compte de l''opérateur sur l''application du noyau Orléanais.';
	COMMENT ON CONSTRAINT c_uni_ope_mail ON symphonie.t_operateur_ope IS
	'Unicité concernant l''adresse mail de l''opérateur.';
	ALTER TABLE symphonie.t_operateur_ope OWNER TO :proprietaire;
	
	-- TABLE : droit
	CREATE TABLE symphonie.t_droit_dro (
		dro_id              SERIAL				NOT NULL,
		dro_code            VARCHAR(5)			NOT NULL,
		dro_libelle         VARCHAR(100)		NOT NULL,
		dro_description     VARCHAR(255)		NULL,
		CONSTRAINT c_uni_dro_code UNIQUE (dro_code),
		CONSTRAINT t_droit_dro_pkey PRIMARY KEY (dro_id)
	);
	COMMENT ON TABLE symphonie.t_droit_dro IS
	'Table contenant tous les droits pouvant être attribués aux utilisateurs de Symphonie.';
	COMMENT ON COLUMN symphonie.t_droit_dro.dro_id IS
	'Squence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_droit_dro.dro_code IS
	'Code du droit.';
	COMMENT ON COLUMN symphonie.t_droit_dro.dro_libelle IS
	'Libellé du droit.';
	COMMENT ON COLUMN symphonie.t_droit_dro.dro_description IS
	'Description du droit.';
	ALTER TABLE symphonie.t_droit_dro OWNER TO :proprietaire;

	-- TABLE : infrastructure
	CREATE TABLE symphonie.t_infrastructure_infra (
		infra_id			SERIAL               NOT NULL,
		infra_code			VARCHAR(32)          NOT NULL,
		infra_description	VARCHAR(255)         NOT NULL,
		CONSTRAINT c_uni_infra_code UNIQUE (infra_code),
		CONSTRAINT t_infrastructure_infra_pkey PRIMARY KEY (infra_id)
	);
	COMMENT ON TABLE symphonie.t_infrastructure_infra IS
	'Table contenant toutes les infrastructures auxquelles peuvent appartenir les génotypes (Exemple : rare).';
	COMMENT ON COLUMN symphonie.t_infrastructure_infra.infra_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_infrastructure_infra.infra_code IS
	'Code de l''infrastructure.';
	COMMENT ON COLUMN symphonie.t_infrastructure_infra.infra_description IS
	'Description de l''infrastructure.';
	ALTER TABLE symphonie.t_infrastructure_infra OWNER TO :proprietaire;

	-- TABLE : convention
	CREATE TABLE symphonie.t_convention_conv (
		conv_id				SERIAL               NOT NULL,
		conv_conv_id		INT4                 NULL,
		conv_nom			VARCHAR(32)          NOT NULL,
		conv_debut			DATE                 NOT NULL,
		conv_fin			DATE                 NOT NULL,
		conv_url			VARCHAR(255)         NOT NULL,
		conv_avenant		bool                 NOT NULL,
		conv_commentaire	VARCHAR(255)         NULL,
		CONSTRAINT c_uni_conv_nom UNIQUE (conv_nom),
		CONSTRAINT c_uni_conv_url UNIQUE (conv_url),
		CONSTRAINT t_convention_conv_pkey PRIMARY KEY (conv_id),
		CONSTRAINT c_fk_conv_conv FOREIGN KEY (conv_conv_id)
			REFERENCES symphonie.t_convention_conv(conv_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE

	);
	COMMENT ON TABLE symphonie.t_convention_conv IS
	'Table contenant toutes les conventions.';
	COMMENT ON COLUMN symphonie.t_convention_conv.conv_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_convention_conv.conv_conv_id IS
	'Rattachement d''une convention à une convention mère.';
	COMMENT ON COLUMN symphonie.t_convention_conv.conv_nom IS
	'Nom de la convention.';
	COMMENT ON COLUMN symphonie.t_convention_conv.conv_debut IS
	'Date de début de la convention.';
	COMMENT ON COLUMN symphonie.t_convention_conv.conv_fin IS
	'Date de fin de la convention.';
	COMMENT ON COLUMN symphonie.t_convention_conv.conv_url IS
	'Url du fichier concernant la convention.';
	COMMENT ON COLUMN symphonie.t_convention_conv.conv_avenant IS
	'Indique s''il s''agit d''un avenant ou pas.';
	COMMENT ON COLUMN symphonie.t_convention_conv.conv_commentaire IS
	'Commentaire concernant la convention.';
	COMMENT ON CONSTRAINT c_fk_conv_conv ON symphonie.t_convention_conv IS
	'Rattachement d''une commune à une et une seule région.';
	ALTER TABLE symphonie.t_convention_conv OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_conv_conv_id_conv
	CREATE INDEX x_btr_conv_conv_id_conv ON symphonie.t_convention_conv USING BTREE (conv_conv_id);
	COMMENT ON INDEX symphonie.x_btr_conv_conv_id_conv IS 
	'Index sur le rattachement à une convention afin d''optimiser le temps des requètes';

	-- TABLE : dispositif
	CREATE TABLE symphonie.t_dispositif_disp (
		disp_id							SERIAL				NOT NULL,
		disp_drai_id					INT4				NULL,
		disp_posb_id					INT4				NOT NULL,
		disp_sylv_id					INT4				NULL,
		disp_acti_id					INT4				NULL,
		disp_oril_id					INT4				NOT NULL,
		disp_typd_id					INT4				NULL,
		disp_typp_id					INT4				NOT NULL,
		disp_code_experience			VARCHAR(20)			NOT NULL,
		disp_nom						VARCHAR(32)			NOT NULL,
		disp_nom_court					VARCHAR(32)			NOT NULL,
		disp_nb_rep						VARCHAR(100)		NULL,
		disp_nb_bloc					VARCHAR(50)			NULL,
		disp_nb_facteur					INT4				NOT NULL,
		disp_nb_traitement				VARCHAR(100)		NULL,
		disp_nb_trt_bloc				VARCHAR(50)			NULL,
		disp_nb_plant_pu				VARCHAR(50)			NULL,
		disp_annee_plantation			VARCHAR(4)			NOT NULL,
		disp_mois_plantation			VARCHAR(2)			NULL,
		disp_annee_preparation			VARCHAR(4)			NULL,
		disp_surf_ini_disp				DECIMAL(5,2)		NULL,
		disp_surf_ini_hors_disp			DECIMAL(5,2)		NULL,
		disp_nb_plant_ini_disp			VARCHAR(100)		NULL,
		disp_nb_plant_ini_hors_disp		VARCHAR(50)			NULL,
		disp_distance_lignes			DOUBLE PRECISION	NOT NULL,
		disp_distance_plants			DOUBLE PRECISION	NOT NULL,
		disp_commentaire				TEXT				NULL,
		CONSTRAINT c_uni_disp_code_experience UNIQUE (disp_code_experience),
		CONSTRAINT c_uni_disp_nom UNIQUE (disp_nom),
		CONSTRAINT t_dispositif_disp_pkey PRIMARY KEY (disp_id), 
		CONSTRAINT c_fk_drai_disp FOREIGN KEY (disp_drai_id)
			REFERENCES symphonie.t_drainage_drai(drai_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE, 
		CONSTRAINT c_fk_posb_disp FOREIGN KEY (disp_posb_id)
			REFERENCES symphonie.t_position_balise_posb(posb_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE, 
		CONSTRAINT c_fk_sylv_disp FOREIGN KEY (disp_sylv_id)
			REFERENCES symphonie.t_sylvo_ecoregion_sylv(sylv_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE, 
		CONSTRAINT c_fk_acti_disp FOREIGN KEY (disp_acti_id)
			REFERENCES symphonie.t_activite_acti(acti_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE, 
		CONSTRAINT c_fk_oril_disp FOREIGN KEY (disp_oril_id)
			REFERENCES symphonie.t_orientation_ligne_oril(oril_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE, 
		CONSTRAINT c_fk_typd_disp FOREIGN KEY (disp_typd_id)
			REFERENCES symphonie.t_type_dispositif_typd(typd_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE, 
		CONSTRAINT c_fk_typp_disp FOREIGN KEY (disp_typp_id)
			REFERENCES symphonie.t_type_propriete_typp(typp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.t_dispositif_disp IS
	'Table contenant tous les dispositifs.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_drai_id IS
	'Drainage associé au dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_posb_id IS
	'Position de la balise au sein du dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_sylv_id IS
	'Sylvo écorégion à laquelle appartient le dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_acti_id IS
	'Niveau d''activité du sispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_oril_id IS
	'Orientation des ligne à l''intérieur du dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_typd_id IS
	'Type de dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_typp_id IS
	'Type de propriété sur laquelle est implenté le dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_code_experience IS
	'Code experience du dipositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_nom IS
	'Nom du dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_nom_court IS
	'Nom court du dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_nb_rep IS
	'Nombre de répétition à l''intérieur du dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_nb_bloc IS
	'Nombre de blocs qui composent le dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_nb_facteur IS
	'Nombre de facteurs qui composent le dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_nb_traitement IS
	'Nombre de traitements qui composent le dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_nb_trt_bloc IS
	'Nombre de traitement par bloc.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_nb_plant_pu IS
	'Nombre de plants par parcelle unitaire.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_annee_plantation IS
	'Année de plantation du dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_mois_plantation IS
	'mois de plantation du dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_annee_preparation IS
	'Année de préparation du dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_surf_ini_disp IS
	'Suface initiale du dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_surf_ini_hors_disp IS
	'Surface initiale hors dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_nb_plant_ini_disp IS
	'Nombre de plants initial dans le dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_nb_plant_ini_hors_disp IS
	'Nombre de plants initial hors dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_distance_lignes IS
	'Distance entre les lignes du dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_distance_plants IS
	'Distance qui sépare chanque individus du dispositif.';
	COMMENT ON COLUMN symphonie.t_dispositif_disp.disp_commentaire IS
	'Commentaire concernant le dispositif.';
	COMMENT ON CONSTRAINT c_fk_drai_disp ON symphonie.t_dispositif_disp IS
	'Rattachement d''un drainage à un dispositif.';
	COMMENT ON CONSTRAINT c_fk_posb_disp ON symphonie.t_dispositif_disp IS
	'Rattachement d''une position de balise à un dispositif.';
	COMMENT ON CONSTRAINT c_fk_sylv_disp ON symphonie.t_dispositif_disp IS
	'Rattachement d''une sylvo-écoregion à un dispositif.';
	COMMENT ON CONSTRAINT c_fk_acti_disp ON symphonie.t_dispositif_disp IS
	'Rattachement d''une activité à un dispositif.';
	COMMENT ON CONSTRAINT c_fk_oril_disp ON symphonie.t_dispositif_disp IS
	'Rattachement d''une orientation de ligne à un dispositif.';
	COMMENT ON CONSTRAINT c_fk_typd_disp ON symphonie.t_dispositif_disp IS
	'Rattachement d''un type de dispositif à un dispositif.';
	COMMENT ON CONSTRAINT c_fk_typp_disp ON symphonie.t_dispositif_disp IS
	'Rattachement d''un type de propriété à un dispositif.';
	ALTER TABLE symphonie.t_dispositif_disp OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_disp_drai_id_disp
	CREATE INDEX x_btr_disp_drai_id_disp ON symphonie.t_dispositif_disp USING BTREE (disp_drai_id);
	COMMENT ON INDEX symphonie.x_btr_disp_drai_id_disp IS 
	'Index sur le rattachement à un drainage afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_disp_posb_id_disp
	CREATE INDEX x_btr_disp_posb_id_disp ON symphonie.t_dispositif_disp USING BTREE (disp_posb_id);
	COMMENT ON INDEX symphonie.x_btr_disp_posb_id_disp IS 
	'Index sur le rattachement à une position de balise afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_disp_sylv_id_disp
	CREATE INDEX x_btr_disp_sylv_id_disp ON symphonie.t_dispositif_disp USING BTREE (disp_sylv_id);
	COMMENT ON INDEX symphonie.x_btr_disp_sylv_id_disp IS 
	'Index sur le rattachement à une sylvio-écorégion afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_disp_acti_id_disp
	CREATE INDEX x_btr_disp_acti_id_disp ON symphonie.t_dispositif_disp USING BTREE (disp_acti_id);
	COMMENT ON INDEX symphonie.x_btr_disp_acti_id_disp IS 
	'Index sur le rattachement à une activité afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_disp_oril_id_disp
	CREATE INDEX x_btr_disp_oril_id_disp ON symphonie.t_dispositif_disp USING BTREE (disp_oril_id);
	COMMENT ON INDEX symphonie.x_btr_disp_oril_id_disp IS 
	'Index sur le rattachement à une orientation de ligne afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_disp_typd_id_disp
	CREATE INDEX x_btr_disp_typd_id_disp ON symphonie.t_dispositif_disp USING BTREE (disp_typd_id);
	COMMENT ON INDEX symphonie.x_btr_disp_typd_id_disp IS 
	'Index sur le rattachement à un type de dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_disp_typp_id_disp
	CREATE INDEX x_btr_disp_typp_id_disp ON symphonie.t_dispositif_disp USING BTREE (disp_typp_id);
	COMMENT ON INDEX symphonie.x_btr_disp_typp_id_disp IS 
	'Index sur le rattachement à un type de propriété afin d''optimiser le temps des requètes';
	
	-- TABLE : type de metadonnee
	CREATE TABLE symphonie.t_type_metadonnee_typm (
		typm_id              SERIAL               NOT NULL,
		typm_code	         VARCHAR(15)          NOT NULL,
		CONSTRAINT c_uni_typm_code UNIQUE (typm_code),
		CONSTRAINT t_type_metadonnee_typm_pkey PRIMARY KEY (typm_id)
	);
	COMMENT ON TABLE symphonie.t_type_metadonnee_typm IS
	'Table contenant les différents types de métadonnées.';
	COMMENT ON COLUMN symphonie.t_type_metadonnee_typm.typm_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_type_metadonnee_typm.typm_code IS
	'Code du type de métadonnée.';
	ALTER TABLE symphonie.t_type_metadonnee_typm OWNER TO :proprietaire;
	
	-- TABLE : metadonnee
	CREATE TABLE symphonie.t_metadonnee_meta (
		meta_id				SERIAL			NOT NULL,
		meta_typm_id			INT4			NOT NULL,
		meta_code			VARCHAR(6)		NOT NULL,
		meta_libelle			VARCHAR(100)	NOT NULL,
		meta_description		VARCHAR(255)    NULL,
		CONSTRAINT c_uni_meta_code UNIQUE (meta_code),
		CONSTRAINT t_metadonnee_meta_pkey PRIMARY KEY (meta_id),
		CONSTRAINT c_fk_typm_meta FOREIGN KEY (meta_typm_id)
			REFERENCES symphonie.t_type_metadonnee_typm(typm_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE

	);
	COMMENT ON TABLE symphonie.t_metadonnee_meta IS
	'Table contenant toutes les métadonnées.';
	COMMENT ON COLUMN symphonie.t_metadonnee_meta.meta_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_metadonnee_meta.meta_typm_id IS
	'Type de la métadonnée concernée.';
	COMMENT ON COLUMN symphonie.t_metadonnee_meta.meta_code IS
	'Code de la métadonnée.';
	COMMENT ON COLUMN symphonie.t_metadonnee_meta.meta_libelle IS
	'Libellé de la métadonnée.';
	COMMENT ON COLUMN symphonie.t_metadonnee_meta.meta_description IS
	'Description de la métadonnée.';
	COMMENT ON CONSTRAINT c_fk_typm_meta ON symphonie.t_metadonnee_meta IS
	'Rattachement d''un	type de métadonnée à une métadonnée.';
	ALTER TABLE symphonie.t_metadonnee_meta OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_meta_typm_id_meta
	CREATE INDEX x_btr_meta_typm_id_meta ON symphonie.t_metadonnee_meta USING BTREE (meta_typm_id);
	COMMENT ON INDEX symphonie.x_btr_meta_typm_id_meta IS 
	'Index sur le rattachement à un type de métadonnée afin d''optimiser le temps des requètes';

	-- TABLE : dépositaire
	CREATE TABLE symphonie.t_depositaire_depo (
		depo_id			SERIAL			NOT NULL,
		depo_nom		VARCHAR(255)	NOT NULL,
		CONSTRAINT c_uni_depo_nom UNIQUE (depo_nom),
		CONSTRAINT t_depositaire_depo_pkey PRIMARY KEY (depo_id)
	);
	COMMENT ON TABLE symphonie.t_depositaire_depo IS
	'Table contenant les dépositaires de génotypes.';
	COMMENT ON COLUMN symphonie.t_depositaire_depo.depo_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_depositaire_depo.depo_nom IS
	'Nom du dépositaire.';
	ALTER TABLE symphonie.t_depositaire_depo OWNER TO :proprietaire;

	-- TABLE : genotype
	CREATE TABLE symphonie.t_genotype_gen (
		gen_id              SERIAL			NOT NULL,
		gen_esp_id			INT4			NOT NULL,
		gen_depo_id			INT4			NOT NULL,
		gen_code			VARCHAR(50)		NOT NULL,
		gen_nom				VARCHAR(100)	NOT NULL,
		gen_lettre_type		CHAR			NULL,
		CONSTRAINT c_uni_gen_code_esp_depo UNIQUE (gen_code, gen_esp_id, gen_depo_id),
		CONSTRAINT t_genotype_gen_pkey PRIMARY KEY (gen_id),
		CONSTRAINT c_fk_esp_gen FOREIGN KEY (gen_esp_id)
			REFERENCES symphonie.tr_espece_esp(esp_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_depo_gen FOREIGN KEY (gen_depo_id)
			REFERENCES symphonie.t_depositaire_depo(depo_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.t_genotype_gen IS
	'Table contenant tous les génotypes.';
	COMMENT ON COLUMN symphonie.t_genotype_gen.gen_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_genotype_gen.gen_esp_id IS
	'Rattachement d''une espèce à un génotype.';
	COMMENT ON COLUMN symphonie.t_genotype_gen.gen_depo_id IS
	'Rattachement d''un dépositaire à un génotype.';
	COMMENT ON COLUMN symphonie.t_genotype_gen.gen_code IS
	'Code du génotype.';
	COMMENT ON COLUMN symphonie.t_genotype_gen.gen_nom IS
	'Nom du génotype.';
	COMMENT ON COLUMN symphonie.t_genotype_gen.gen_lettre_type IS
	'Lettre type du génotype (Exemple : C = croisement).';
	COMMENT ON CONSTRAINT c_fk_esp_gen ON symphonie.t_genotype_gen IS
	'Rattachement d''une espèce à un génotype.';
	COMMENT ON CONSTRAINT c_fk_depo_gen ON symphonie.t_genotype_gen IS
	'Rattachement d''un dépositaire à un génotype.';
	ALTER TABLE symphonie.t_genotype_gen OWNER TO :proprietaire;

	-- création de l'Index : x_btrgen_esp_id_gen
	CREATE INDEX x_btrgen_esp_id_gen ON symphonie.t_genotype_gen USING BTREE (gen_esp_id);
	COMMENT ON INDEX symphonie.x_btrgen_esp_id_gen IS 
	'Index sur le rattachement à une espèce afin d''optimiser le temps des requètes';

	-- création de l'Index : x_btr_gen_depo_id_gen
	CREATE INDEX x_btr_gen_depo_id_gen ON symphonie.t_genotype_gen USING BTREE (gen_depo_id);
	COMMENT ON INDEX symphonie.x_btr_gen_depo_id_gen IS 
	'Index sur le rattachement à un dépositaire afin d''optimiser le temps des requètes';
	
	-- TABLE : element
	CREATE TABLE symphonie.t_element_elem (
		elem_id					SERIAL					NOT NULL,
		elem_gen_id				INT4					NULL,
		elem_disp_id			INT4					NULL,
		elem_refg_id			INT4					NULL,
		elem_nom_court			VARCHAR(50)				NOT NULL,
		elem_nom				VARCHAR(150)			NOT NULL,
		elem_code_barre			VARCHAR(50)				NULL,
		elem_x_relatif			INT4					NULL,
		elem_y_relatif			INT4					NULL,
		elem_x_geographique		DOUBLE PRECISION		NULL,
		elem_y_geographique		DOUBLE PRECISION		NULL,
		elem_num_bloc			VARCHAR(10)				NULL,
		elem_num_rep			VARCHAR(5)				NULL,
		elem_num_pu				VARCHAR(10)				NULL,
		elem_facteur_1			VARCHAR(255)			NULL,
		elem_facteur_2			VARCHAR(255)			NULL,
		elem_facteur_3			VARCHAR(255)			NULL,
		--CONSTRAINT c_uni_elem_nom_court UNIQUE (elem_nom_court),
		CONSTRAINT t_element_elem_pkey PRIMARY KEY (elem_id),
		CONSTRAINT c_fk_gen_elem FOREIGN KEY (elem_gen_id)
			REFERENCES symphonie.t_genotype_gen(gen_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_disp_elem FOREIGN KEY (elem_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_refg_elem FOREIGN KEY (elem_refg_id)
			REFERENCES symphonie.tr_referentiel_geographique_refg(refg_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	
	COMMENT ON TABLE symphonie.t_element_elem IS
	'Table contenant tous les éléments (individus).';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_gen_id IS
	'Identifiant du génotype rattaché à l''élément (individu).';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_disp_id IS
	'Identifiant du dispositif auquel l''élément (individu) est rattaché.';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_refg_id IS
	'Identifiant du référentiel géographique auquel l''élément (individu) est rattaché.';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_nom_court IS
	'Nom court de l''élément (individu).';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_nom IS
	'Nom de l''élément (individu).';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_code_barre IS
	'Code barre de l''élément (individu).';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_x_relatif IS
	'Position en x d''un élément dans un dispositif (coordonnée relative).';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_y_relatif IS
	'Position en y d''un élément dans un dispositif (coordonnée relative).';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_num_bloc IS
	'Numéro de bloc d''un élément dans un dispositif.';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_num_rep IS
	'Numéro de répétition d''un élément dans un dispositif.';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_num_pu IS
	'Numéro de la parcelle unitaire d''un élément dans un dispositif.';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_x_geographique IS
	'Position en x d''un élément dans un dispositif (coordonnée géographique).';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_y_geographique IS
	'Position en y d''un élément dans un dispositif (coordonnée géographique).';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_facteur_1 IS
	'Facteur N°1 de l''élément.';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_facteur_2 IS
	'Facteur N°2 de l''élément.';
	COMMENT ON COLUMN symphonie.t_element_elem.elem_facteur_3 IS
	'Facteur N°3 de l''élément.';
	COMMENT ON CONSTRAINT c_fk_gen_elem ON symphonie.t_element_elem IS
	'Rattachement d''un	génotype à un élément (individu).';
	COMMENT ON CONSTRAINT c_fk_disp_elem ON symphonie.t_element_elem IS
	'Rattachement d''un	dispositf à un élément.';
	ALTER TABLE symphonie.t_element_elem OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_elem_gen_id_elem
	CREATE INDEX x_btr_elem_gen_id_elem ON symphonie.t_element_elem USING BTREE (elem_gen_id);
	COMMENT ON INDEX symphonie.x_btr_elem_gen_id_elem IS
	'Index sur le rattachement à un génotype afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_elem_disp_id_elem
	CREATE INDEX x_btr_elem_disp_id_elem ON symphonie.t_element_elem USING BTREE (elem_disp_id);
	COMMENT ON INDEX symphonie.x_btr_elem_disp_id_elem IS 
	'Index sur le rattachement à un dispositif afin d''optimiser le temps des requètes';

	-- création de l'Index : x_btr_elem_refg_id_elem
	CREATE INDEX x_btr_elem_refg_id_elem ON symphonie.t_element_elem USING BTREE (elem_refg_id);
	COMMENT ON INDEX symphonie.x_btr_elem_refg_id_elem IS 
	'Index sur le rattachement à un référentiel géographique afin d''optimiser le temps des requètes';

	-- création de la colonne contenant l''objet géométrique
	SELECT addgeometrycolumn('symphonie','t_element_elem', 'geom', 2154, 'POINT', 2, false);

	-- TABLE : type de fichier
	CREATE TABLE symphonie.t_type_fichier_typf (
		typf_id					SERIAL			NOT NULL,
		typf_nom				VARCHAR(255)	NOT NULL,
		typf_job_integration	VARCHAR(255)	NOT NULL,
		typf_dty_id				INT4			NULL,
		CONSTRAINT t_type_fichier_typs_pkey PRIMARY KEY (typf_id),
		CONSTRAINT c_uni_typf_nom UNIQUE (typf_nom)
	);
	COMMENT ON TABLE symphonie.t_type_fichier_typf IS
	'Table contenant les différents type de fichier qu''il est possible de déposer via le noyau Orléannais.';
	COMMENT ON COLUMN symphonie.t_type_fichier_typf.typf_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_type_fichier_typf.typf_nom IS
	'Nom du type de fichier.';
	COMMENT ON COLUMN symphonie.t_type_fichier_typf.typf_job_integration IS
	'Nom du job talend permettant l''intégration du fichier en base.';
	COMMENT ON COLUMN symphonie.t_type_fichier_typf.typf_dty_id IS
	'Identifiant du type de données (datatype dans la base du noyau).';
	ALTER TABLE symphonie.t_type_fichier_typf OWNER TO :proprietaire;
	
	-- TABLE : fichier
	CREATE TABLE symphonie.t_fichier_fic (
		fic_id          			SERIAL			NOT NULL,
		fic_typf_id					INT4			NOT NULL,
		fic_ids_id					INT4			NULL,
		fic_ope_id_publication		INT4			NULL,
		fic_nom         			VARCHAR(120)	NULL,
		fic_publie					BOOLEAN			NOT NULL	DEFAULT false,
		fic_version_publiee			INT4			NULL,
		fic_publication_date		DATE			NULL,
		fic_ajout_via_noyau			BOOLEAN			NOT NULL	DEFAULT true,
		fic_blob					INT4			NULL,
		fic_ope_id_validation		INT4			NULL,
		fic_validation_date			DATE			NULL,
		CONSTRAINT c_uni_fic_nom UNIQUE (fic_nom),
		CONSTRAINT c_uni_fic_blob UNIQUE (fic_blob),
		CONSTRAINT t_fichier_fic_pkey PRIMARY KEY (fic_id),
		CONSTRAINT c_fk_fic_typf_id FOREIGN KEY (fic_typf_id)
			REFERENCES symphonie.t_type_fichier_typf(typf_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_fic_ope_id_validation FOREIGN KEY (fic_ope_id_validation)
			REFERENCES symphonie.t_operateur_ope(ope_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.t_fichier_fic IS
	'Table contenant tous les protocoles.';
	COMMENT ON COLUMN symphonie.t_fichier_fic.fic_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_fichier_fic.fic_typf_id IS
	'Rattachement d''un type de fichier.';
	COMMENT ON COLUMN symphonie.t_fichier_fic.fic_ids_id IS
	'Identifiant postgres du jeu de donnée (base noyau autonome) concerné par ce fichier.';
	COMMENT ON COLUMN symphonie.t_fichier_fic.fic_ope_id_publication IS
	'Identifiant (au sens postgres) du compte (dans la base de donnée du noyau autonome) de l''utilisateur qui a publié le fichier sur le noyau. Donc dans la base de données Symphonie il s''agit de l''opérateur ayant validé le fichier.';
	COMMENT ON COLUMN symphonie.t_fichier_fic.fic_nom IS
	'Nom du fichier.';
	COMMENT ON COLUMN symphonie.t_fichier_fic.fic_publie IS
	'Boolean indiquant si le fichier est publié.';
	COMMENT ON COLUMN symphonie.t_fichier_fic.fic_version_publiee IS
	'Numéro de la version du fichier qui est publiée.';
	COMMENT ON COLUMN symphonie.t_fichier_fic.fic_publication_date IS
	'Date et heure de  publication du fichier dans la base de données Symphonie.';
	COMMENT ON COLUMN symphonie.t_fichier_fic.fic_ajout_via_noyau IS
	'Boolean indiquant si le fichier a été rajouté via le noyau (true) ou par un autre processus (false).';
	COMMENT ON COLUMN symphonie.t_fichier_fic.fic_blob IS
	'Numéro oid du fichier concerné.';
	COMMENT ON COLUMN symphonie.t_fichier_fic.fic_ope_id_validation IS
	'Rattachement de l''opérateur ayant publié le fichier.';
	COMMENT ON COLUMN symphonie.t_fichier_fic.fic_validation_date IS
	'Date et heure de  validation du fichier, donc la date et l''heur de la publication du fichier dans la base de données du noyau autonome.';
	COMMENT ON CONSTRAINT c_fk_fic_typf_id ON symphonie.t_fichier_fic IS
	'Rattachement d''un	type de fichier au fichier.';
	COMMENT ON CONSTRAINT c_fk_fic_ope_id_validation ON symphonie.t_fichier_fic IS
	'Rattachement de l''opérateur ayant validé le fichier.';
	ALTER TABLE symphonie.t_fichier_fic OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_fic_typf_id_fic
	CREATE INDEX x_btr_fic_typf_id_fic ON symphonie.t_fichier_fic USING BTREE (fic_typf_id);
	COMMENT ON INDEX symphonie.x_btr_fic_typf_id_fic IS
	'Index sur le rattachement du type de fichier afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_fic_ope_id_publication
	CREATE INDEX x_btr_fic_ope_id_validation ON symphonie.t_fichier_fic USING BTREE (fic_ope_id_validation);
	COMMENT ON INDEX symphonie.x_btr_fic_ope_id_validation IS
	'Index sur le rattachement a l''opérateur qui a validé le fichier afin d''optimiser le temps des requètes';
	
	-- TABLE : variable
	CREATE TABLE symphonie.t_variable_var (
		var_id				SERIAL			NOT NULL,
		var_ont_id			INT4			NULL,
		var_code			VARCHAR(20)		NOT NULL,
		var_nom				VARCHAR(100)	NOT NULL,
		var_description		TEXT			NULL,
		CONSTRAINT t_variable_var_pkey PRIMARY KEY (var_id),
		CONSTRAINT c_uni_var_code UNIQUE (var_code),
		CONSTRAINT c_fk_ont_var FOREIGN KEY (var_ont_id)
			REFERENCES symphonie.tr_ontologie_ont(ont_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.t_variable_var IS
	'Table contenant les variables mesurées.';
	COMMENT ON COLUMN symphonie.t_variable_var.var_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_variable_var.var_ont_id IS
	'Rattachement d''une variable provenant de l''ontologie à une variable mesurée.';
	COMMENT ON COLUMN symphonie.t_variable_var.var_code IS
	'Code de la variable (ex : deb921).';
	COMMENT ON COLUMN symphonie.t_variable_var.var_nom IS
	'Nom de la variable (ex : débourrement à l''œil nu).';
	COMMENT ON COLUMN symphonie.t_variable_var.var_description IS
	'Description de la variable.';
	COMMENT ON CONSTRAINT c_fk_ont_var ON symphonie.t_variable_var IS
	'Rattachement d''une variable ISsue de l''ontologie à une variable.';
	ALTER TABLE symphonie.t_variable_var OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_var_ont_id_var
	CREATE INDEX x_btr_var_ont_id_var ON symphonie.t_variable_var USING BTREE (var_ont_id);
	COMMENT ON INDEX symphonie.x_btr_var_ont_id_var IS 
	'Index sur le rattachement à une variable de l''ontologie afin d''optimiser le temps des requètes';
	
	-- TABLE : regroupemnt de mesure
	CREATE TABLE symphonie.t_regroupement_mesure_rgpm (
		rgpm_id				SERIAL			NOT NULL,
		rgpm_disp_id		INT4			NOT NULL,
		rgpm_rgpm_id		INT4			NULL,
		rgpm_norm_id		INT4			NULL,
		rgpm_met_id			INT4			NULL,
		rgpm_uni_id			INT4			NULL,
		rgpm_fic_id			INT4			NULL,
		rgpm_var_id			INT4			NULL,
		rgpm_code			VARCHAR(20)		NOT NULL,
		rgpm_nom			VARCHAR(150)	NOT NULL,
		rgpm_description	TEXT            NULL,
		CONSTRAINT t_regroupement_mesure_rgpm_pkey PRIMARY KEY (rgpm_id),
		CONSTRAINT c_fk_disp_rgpm FOREIGN KEY (rgpm_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_rgpm_rgpm FOREIGN KEY (rgpm_rgpm_id)
			REFERENCES symphonie.t_regroupement_mesure_rgpm(rgpm_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_norm_rgpm FOREIGN KEY (rgpm_norm_id)
			REFERENCES symphonie.tr_norme_norm(norm_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_met_rgpm FOREIGN KEY (rgpm_met_id)
			REFERENCES symphonie.tr_methode_met(met_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_uni_rgpm FOREIGN KEY (rgpm_uni_id)
			REFERENCES symphonie.tr_unite_uni(uni_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_fic_rgpm FOREIGN KEY (rgpm_fic_id)
			REFERENCES symphonie.t_fichier_fic(fic_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_var_rgpm FOREIGN KEY (rgpm_var_id)
			REFERENCES symphonie.t_variable_var(var_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	
	COMMENT ON TABLE symphonie.t_regroupement_mesure_rgpm IS
	'Table contenant tous les regroupement de mesure.';
	COMMENT ON COLUMN symphonie.t_regroupement_mesure_rgpm.rgpm_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_regroupement_mesure_rgpm.rgpm_disp_id IS
	'Identifiant du dispositif concerné par le regroupement de mesure.';
	COMMENT ON COLUMN symphonie.t_regroupement_mesure_rgpm.rgpm_rgpm_id IS
	'Identifiant du regroupement de mesure auquel appartient le regroupement de mesure concerné.';
	COMMENT ON COLUMN symphonie.t_regroupement_mesure_rgpm.rgpm_norm_id IS
	'Identifiant de la norme rattachée au regroupement de mesure.';
	COMMENT ON COLUMN symphonie.t_regroupement_mesure_rgpm.rgpm_met_id IS
	'Identifiant de la méthode rattachée au regroupement de mesure.';
	COMMENT ON COLUMN symphonie.t_regroupement_mesure_rgpm.rgpm_uni_id IS
	'Identifiant de l''unité rattachée au regroupement de mesure.';
	COMMENT ON COLUMN symphonie.t_regroupement_mesure_rgpm.rgpm_fic_id IS
	'Identifiant du fichier rattaché au regroupement de mesure.';
	COMMENT ON COLUMN symphonie.t_regroupement_mesure_rgpm.rgpm_var_id IS
	'Identifiant de la variable rattachée au regroupement de mesure.';
	COMMENT ON COLUMN symphonie.t_regroupement_mesure_rgpm.rgpm_code IS
	'Code du regroupement de mesure.';
	COMMENT ON COLUMN symphonie.t_regroupement_mesure_rgpm.rgpm_nom IS
	'Nom du regroupement de mesure.';
	COMMENT ON COLUMN symphonie.t_regroupement_mesure_rgpm.rgpm_description IS
	'Description du regroupement de mesure.';
	COMMENT ON CONSTRAINT c_fk_disp_rgpm ON symphonie.t_regroupement_mesure_rgpm IS
	'Rattachement d''un	dispositif à un regroupement de mesure.';
	COMMENT ON CONSTRAINT c_fk_rgpm_rgpm ON symphonie.t_regroupement_mesure_rgpm IS
	'Rattachement d''un regroupement de mesure à un regroupment de mesure.';
	COMMENT ON CONSTRAINT c_fk_norm_rgpm ON symphonie.t_regroupement_mesure_rgpm IS
	'Rattachement d''une norme à une mesure.';
	COMMENT ON CONSTRAINT c_fk_met_rgpm ON symphonie.t_regroupement_mesure_rgpm IS
	'Rattachement d''une méthode à un regroupement de mesure.';
	COMMENT ON CONSTRAINT c_fk_uni_rgpm ON symphonie.t_regroupement_mesure_rgpm IS
	'Rattachement d''une unité à un regroupement de mesure.';
	COMMENT ON CONSTRAINT c_fk_fic_rgpm ON symphonie.t_regroupement_mesure_rgpm IS
	'Rattachement d''un fichier à un regroupement de mesure.';
	COMMENT ON CONSTRAINT c_fk_var_rgpm ON symphonie.t_regroupement_mesure_rgpm IS
	'Rattachement d''une variable à un regroupement de mesure.';
	ALTER TABLE symphonie.t_regroupement_mesure_rgpm OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_rgpm_disp_id_rgpm
	CREATE INDEX x_btr_rgpm_disp_id_rgpm ON symphonie.t_regroupement_mesure_rgpm USING BTREE (rgpm_disp_id);
	COMMENT ON INDEX symphonie.x_btr_rgpm_disp_id_rgpm IS 
	'Index sur le rattachement à un dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_rgpm_rgpm_id_rgpm
	CREATE INDEX x_btr_rgpm_rgpm_id_rgpm ON symphonie.t_regroupement_mesure_rgpm USING BTREE (rgpm_rgpm_id);
	COMMENT ON INDEX symphonie.x_btr_rgpm_rgpm_id_rgpm IS 
	'Index sur le rattachement à un regroupement de mesures afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_rgpm_norm_id_rgpm
	CREATE INDEX x_btr_rgpm_norm_id_rgpm ON symphonie.t_regroupement_mesure_rgpm USING BTREE (rgpm_norm_id);
	COMMENT ON INDEX symphonie.x_btr_rgpm_norm_id_rgpm IS 
	'Index sur le rattachement à une norme afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_rgpm_met_id_rgpm
	CREATE INDEX x_btr_rgpm_met_id_rgpm ON symphonie.t_regroupement_mesure_rgpm USING BTREE (rgpm_met_id);
	COMMENT ON INDEX symphonie.x_btr_rgpm_met_id_rgpm IS 
	'Index sur le rattachement à une méthode afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_rgpm_uni_id_rgpm
	CREATE INDEX x_btr_rgpm_uni_id_rgpm ON symphonie.t_regroupement_mesure_rgpm USING BTREE (rgpm_uni_id);
	COMMENT ON INDEX symphonie.x_btr_rgpm_disp_id_rgpm IS 
	'Index sur le rattachement à une unité afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_rgpm_fic_id_rgpm
	CREATE INDEX x_btr_rgpm_fic_id_rgpm ON symphonie.t_regroupement_mesure_rgpm USING BTREE (rgpm_fic_id);
	COMMENT ON INDEX symphonie.x_btr_rgpm_fic_id_rgpm IS 
	'Index sur le rattachement à un fichier afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_rgpm_var_id_rgpm
	CREATE INDEX x_btr_rgpm_var_id_rgpm ON symphonie.t_regroupement_mesure_rgpm USING BTREE (rgpm_var_id);
	COMMENT ON INDEX symphonie.x_btr_rgpm_var_id_rgpm IS 
	'Index sur le rattachement à une variable afin d''optimiser le temps des requètes';
	
	-- TABLE : mesure
	CREATE TABLE symphonie.t_mesure_mes (
		mes_id					SERIAL			NOT NULL,
		mes_rgpm_id				INT4			NULL,
		mes_uni_id				INT4			NULL,
		mes_norm_id				INT4			NULL,
		mes_met_id				INT4			NULL,
		mes_fic_id				INT4			NULL,
		mes_var_id				INT4			NOT NULL,
		mes_elem_id				INT4			NOT NULL,
		mes_valeur_typ_reel		FLOAT8			NULL,
		mes_valeur_typ_alpha	VARCHAR(255)	NULL,
		mes_valeur_typ_date		DATE			NULL,
		mes_date_mesure			DATE			NOT NULL,
		mes_remarque			VARCHAR(255)	NULL,
		mes_precision_valeur	INT4			NULL,
		mes_precision_date		INT4			NULL,
		CONSTRAINT t_mesure_mes_pkey PRIMARY KEY (mes_id),
		CONSTRAINT c_fk_rgpm_mes FOREIGN KEY (mes_rgpm_id)
			REFERENCES symphonie.t_regroupement_mesure_rgpm(rgpm_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_uni_mes FOREIGN KEY (mes_uni_id)
			REFERENCES symphonie.tr_unite_uni(uni_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_norm_mes FOREIGN KEY (mes_norm_id)
			REFERENCES symphonie.tr_norme_norm(norm_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_met_mes FOREIGN KEY (mes_met_id)
			REFERENCES symphonie.tr_methode_met(met_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_fic_mes FOREIGN KEY (mes_fic_id)
			REFERENCES symphonie.t_fichier_fic(fic_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_var_mes FOREIGN KEY (mes_var_id)
			REFERENCES symphonie.t_variable_var(var_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_elem_mes FOREIGN KEY (mes_elem_id)
			REFERENCES symphonie.t_element_elem(elem_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	
	COMMENT ON TABLE symphonie.t_mesure_mes IS
	'Table contenant toutes les mesures effectuées.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_rgpm_id IS
	'Identifiant du regroupement de mesure auquel appartient la mesure concernée.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_uni_id IS
	'Rattachement d''une unité à une mesure.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_norm_id IS
	'Rattechement d''une norme à une mesure.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_met_id IS
	'Rattachement d''une méthode à une mesure.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_fic_id IS
	'Rattachement d''un fichier à une mesure.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_var_id IS
	'Rattachement d''une variable à une mesure.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_elem_id IS
	'Identifiant de l''élémént rattaché à la mesure.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_valeur_typ_reel IS
	'Valeur de la mesure au format reel coté postgres.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_valeur_typ_alpha IS
	'Valeur de la mesure au format alphanumérique coté postgres.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_valeur_typ_date IS
	'Valeur de la mesure au format date coté postgres.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_date_mesure IS
	'Date de la mesure au format (jj-mm-aaaa).';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_remarque IS
	'Remarque concernant la mesure.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_precision_valeur IS
	'Précision de la valeur de la mesure exprimée dans la même unité que la mesure.';
	COMMENT ON COLUMN symphonie.t_mesure_mes.mes_precision_date IS
	'Précision de la date de la mesure exprimée en jours (ex :15 = à plus ou moins 15 jours prés).';
	COMMENT ON CONSTRAINT c_fk_rgpm_mes ON symphonie.t_mesure_mes IS
	'Rattachement d''un	regroupement de mesure à une mesure.';
	COMMENT ON CONSTRAINT c_fk_uni_mes ON symphonie.t_mesure_mes IS
	'Rattachement d''une unité à une mesure.';
	COMMENT ON CONSTRAINT c_fk_norm_mes ON symphonie.t_mesure_mes IS
	'Rattachement d''une norme à une mesure.';
	COMMENT ON CONSTRAINT c_fk_met_mes ON symphonie.t_mesure_mes IS
	'Rattachement d''une méthode à une mesure.';
	COMMENT ON CONSTRAINT c_fk_fic_mes ON symphonie.t_mesure_mes IS
	'Rattachement d''un	fichier à une mesure.';
	COMMENT ON CONSTRAINT c_fk_var_mes ON symphonie.t_mesure_mes IS
	'Rattachement d''une variable à une mesure.';
	COMMENT ON CONSTRAINT c_fk_elem_mes ON symphonie.t_mesure_mes IS
	'Rattachement d''un	élément (individu) à une mesure.';
	ALTER TABLE symphonie.t_mesure_mes OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_mes_rgpm_id_mes
	CREATE INDEX x_btr_mes_rgpm_id_mes ON symphonie.t_mesure_mes USING BTREE (mes_rgpm_id);
	COMMENT ON INDEX symphonie.x_btr_mes_rgpm_id_mes IS
	'Index sur le rattachement à un regroupement de mesure afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_mes_uni_id_mes
	CREATE INDEX x_btr_mes_uni_id_mes ON symphonie.t_mesure_mes USING BTREE (mes_uni_id);
	COMMENT ON INDEX symphonie.x_btr_mes_uni_id_mes IS
	'Index sur le rattachement à une unité afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_mes_norm_id_mes
	CREATE INDEX x_btr_mes_norm_id_mes ON symphonie.t_mesure_mes USING BTREE (mes_norm_id);
	COMMENT ON INDEX symphonie.x_btr_mes_norm_id_mes IS
	'Index sur le rattachement à une norme afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_mes_met_id_mes
	CREATE INDEX x_btr_mes_met_id_mes ON symphonie.t_mesure_mes USING BTREE (mes_met_id);
	COMMENT ON INDEX symphonie.x_btr_mes_met_id_mes IS
	'Index sur le rattachement à une méthode afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_mes_fic_id_mes
	CREATE INDEX x_btr_mes_fic_id_mes ON symphonie.t_mesure_mes USING BTREE (mes_fic_id);
	COMMENT ON INDEX symphonie.x_btr_mes_fic_id_mes IS
	'Index sur le rattachement à un fichier afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_mes_var_id_mes
	CREATE INDEX x_btr_mes_var_id_mes ON symphonie.t_mesure_mes USING BTREE (mes_var_id);
	COMMENT ON INDEX symphonie.x_btr_mes_rgpm_id_mes IS
	'Index sur le rattachement à une variable afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_mes_elem_id_mes
	CREATE INDEX x_btr_mes_elem_id_mes ON symphonie.t_mesure_mes USING BTREE (mes_elem_id);
	COMMENT ON INDEX symphonie.x_btr_mes_elem_id_mes IS
	'Index sur le rattachement à un élément afin d''optimiser le temps des requètes';

	-- création de l'Index : x_btr_mes_date_mesure_mes
	CREATE INDEX x_btr_mes_date_mesure_mes ON symphonie.t_mesure_mes USING BTREE (mes_date_mesure);
	COMMENT ON INDEX symphonie.x_btr_mes_date_mesure_mes IS 
	'Index sur ladate de la mesure afin d''optimiser le temps des requètes';

	-- TABLE : fichier_operation
	CREATE TABLE symphonie.t_fichier_operation_ficope (
		ficope_id				SERIAL			NOT NULL,
		ficope_operation_id		INT4			NOT NULL,
		ficope_fic_id			INT4			NULL,
		ficope_typf_id			INT4			NOT NULL,
		ficope_ope_id			INT4			NOT NULL,
		ficope_ids_id			BIGINT			NOT NULL,
		ficope_nom_fichier		VARCHAR(255)	NOT NULL,
		ficope_version			BIGINT			NOT NULL,
		ficope_date				DATE			NOT NULL	DEFAULT now(),
		ficope_statut			VARCHAR(15)		NOT NULL	DEFAULT 'en attente',
		ficope_type_operation	VARCHAR(15)		NOT NULL,
		CONSTRAINT t_fichier_operation_ficope_pkey PRIMARY KEY (ficope_id),
		CONSTRAINT c_fk_ficope_typf_id FOREIGN KEY (ficope_typf_id)
			REFERENCES symphonie.t_type_fichier_typf(typf_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_ficope_ope_id FOREIGN KEY (ficope_ope_id)
			REFERENCES symphonie.t_operateur_ope(ope_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.t_fichier_operation_ficope IS
	'Table contenanr les opérations concernant la  publication ou la dé publication des fichiers.';
	COMMENT ON COLUMN symphonie.t_fichier_operation_ficope.ficope_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_fichier_operation_ficope.ficope_operation_id IS
	'Identifiant d''une opération correspondant au traitement de un ou plusieurs fichiers';
	COMMENT ON COLUMN symphonie.t_fichier_operation_ficope.ficope_fic_id IS
	'Fichier a traiter durant l''opération (base de données Symphonie).';
	COMMENT ON COLUMN symphonie.t_fichier_operation_ficope.ficope_typf_id IS
	'Type du fichier a traiter durant l''opération (base de données Symphonie synchronisé avec celle du noyau)).';
	COMMENT ON COLUMN symphonie.t_fichier_operation_ficope.ficope_ope_id IS
	'Opérateur qui a initié l''opération.';
	COMMENT ON COLUMN symphonie.t_fichier_operation_ficope.ficope_ids_id IS
	'Identifiant du jeu de données dans la base du noyau.';
	COMMENT ON COLUMN symphonie.t_fichier_operation_ficope.ficope_nom_fichier IS
	'Nom du fichier (nom de la version du dataset dans la BDD du noyau autonome) concerné par l''opération.';
	COMMENT ON COLUMN symphonie.t_fichier_operation_ficope.ficope_version IS
	'Numéro de la version du fichier a traiter.';
	COMMENT ON COLUMN symphonie.t_fichier_operation_ficope.ficope_date IS
	'Date a laquelle l''operateur a initié cette opération (par défaut il s''agit de la date du jour).';
	COMMENT ON COLUMN symphonie.t_fichier_operation_ficope.ficope_statut IS
	'Statut de l''opération (par défaut le statut vaut : en attente).';
	COMMENT ON COLUMN symphonie.t_fichier_operation_ficope.ficope_type_operation IS
	'Type de l''opération (publication ou dépublication).';
	COMMENT ON CONSTRAINT c_fk_ficope_typf_id ON symphonie.t_fichier_operation_ficope IS
	'Rattachement du type du fichier concerné par l''opération.';
	COMMENT ON CONSTRAINT c_fk_ficope_ope_id ON symphonie.t_fichier_operation_ficope IS
	'Rattachement de l''opérateur ayant initié l''opération.';
	ALTER TABLE symphonie.t_fichier_operation_ficope OWNER TO :proprietaire;

	-- création de l'Index : x_btr_ficope_fic_id_ficope
	CREATE INDEX x_btr_ficope_fic_id_ficope ON symphonie.t_fichier_operation_ficope USING BTREE (ficope_fic_id);
	COMMENT ON INDEX symphonie.x_btr_ficope_fic_id_ficope IS
	'Index sur le rattachement du fichier à l''opération';

	-- création de l'Index : x_btr_ficope_typf_id_ficope
	CREATE INDEX x_btr_ficope_typf_id_ficope ON symphonie.t_fichier_operation_ficope USING BTREE (ficope_typf_id);
	COMMENT ON INDEX symphonie.x_btr_ficope_typf_id_ficope IS
	'Index sur le rattachement du type de fichier (datatype pour la base du noyau) concerné par l''opération';

	-- création de l'Index : x_btr_ficope_ope_id_ficope
	CREATE INDEX x_btr_ficope_ope_id_ficope ON symphonie.t_fichier_operation_ficope USING BTREE (ficope_ope_id);
	COMMENT ON INDEX symphonie.x_btr_ficope_ope_id_ficope IS
	'Index sur le rattachement de l''opérateur ayant initié l''opération';

	-- TABLE : resultat_operation
	CREATE TABLE symphonie.t_resultat_operation_resope (
		resope_id				SERIAL			NOT NULL,
		resope_ficope_id		INT4			NOT NULL,
		resope_num_operation	INT4			NOT NULL,
		resope_date				DATE			NOT NULL	DEFAULT now(),
		resope_reussite			BOOLEAN			NOT NULL,
		resope_type_operation	VARCHAR(20)		NOT NULL,
		resope_nom_fichier		VARCHAR(255)	NOT NULL,
		resope_version			INT4			NOT NULL,
		resope_nom_job			VARCHAR(100)	NULL,
		resope_erreur			TEXT			NULL,
		CONSTRAINT t_resultat_operation_resope_pkey PRIMARY KEY (resope_id)
	);

	COMMENT ON TABLE symphonie.t_resultat_operation_resope IS
	'Table contenanr les résultats des opérations lancées.';
	COMMENT ON COLUMN symphonie.t_resultat_operation_resope.resope_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_resultat_operation_resope.resope_ficope_id IS
	'Identifiant d''une opération correspondant au traitement de un ou plusieurs fichiers';
	COMMENT ON COLUMN symphonie.t_resultat_operation_resope.resope_num_operation IS
	'Numéro de l''opération concernée par le résultat.';
	COMMENT ON COLUMN symphonie.t_resultat_operation_resope.resope_date IS
	'Date du traitement de l''opération.';
	COMMENT ON COLUMN symphonie.t_resultat_operation_resope.resope_reussite IS
	'Boolean indiquant si l''opération a été réussite.';
	COMMENT ON COLUMN symphonie.t_resultat_operation_resope.resope_type_operation IS
	'Type de l''opération concernée par le résultat.';
	COMMENT ON COLUMN symphonie.t_resultat_operation_resope.resope_nom_fichier IS
	'Nom du fichier concerné par l''opération.';
	COMMENT ON COLUMN symphonie.t_resultat_operation_resope.resope_version IS
	'Numéro de la version du fichier concerné par le résultat.';
	COMMENT ON COLUMN symphonie.t_resultat_operation_resope.resope_nom_job IS
	'Nom du job durant lequel l''erreur est survenue si l''opération n''a pas réussie.';
	COMMENT ON COLUMN symphonie.t_resultat_operation_resope.resope_erreur IS
	'Détail sur l''erreur qui est survenue si l''opération n''a pas réussie.';

	ALTER TABLE symphonie.t_resultat_operation_resope OWNER TO :proprietaire;

		-- création de l'Index : x_btr_resope_ficope_id_resope
	CREATE INDEX x_btr_resope_ficope_id_resope ON symphonie.t_resultat_operation_resope USING BTREE (resope_ficope_id);
	COMMENT ON INDEX symphonie.x_btr_resope_ficope_id_resope IS
	'Index sur le rattachement d''une opération au résultat';

	-- TABLE : synonyme (génotypes)
	CREATE TABLE symphonie.t_synonyme_syn (
		syn_id				SERIAL			NOT NULL,
		syn_nom_synonyme	VARCHAR(255)	NOT NULL,
		CONSTRAINT c_uni_syn_nom_synonyme UNIQUE (syn_nom_synonyme),
		CONSTRAINT t_synonyme_syn_pkey PRIMARY KEY (syn_id)
	);
	COMMENT ON TABLE symphonie.t_synonyme_syn IS
	'Table contenant tous les noms synonymes des génotypes.';
	COMMENT ON COLUMN symphonie.t_synonyme_syn.syn_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_synonyme_syn.syn_nom_synonyme IS
	'Nom synonyme.';
	ALTER TABLE symphonie.t_synonyme_syn OWNER TO :proprietaire;

	-- TABLE : bornes
	CREATE TABLE symphonie.t_borne_bor (
		bor_id				SERIAL				NOT NULL,
		bor_disp_id			INT4				NOT NULL,
		bor_com_id			INT4				NOT NULL,
		bor_refg_id			INT4				NOT NULL,
		bor_longitude		DOUBLE PRECISION	NOT NULL,
		bor_latitude		DOUBLE PRECISION	NOT NULL,
		bor_altitude		DOUBLE PRECISION	NULL,
		bor_localisation	VARCHAR(255)		NOT NULL,
		CONSTRAINT t_borne_bor_pkey PRIMARY KEY (bor_id),
		CONSTRAINT c_fk_disp_bor FOREIGN KEY (bor_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_com_bor FOREIGN KEY (bor_com_id)
			REFERENCES symphonie.tr_communes_com(com_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_refg_bor FOREIGN KEY (bor_refg_id)
			REFERENCES symphonie.tr_referentiel_geographique_refg(refg_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.t_borne_bor IS
	'Table contenant toutes les bornes associées aux dispositifs.';
	COMMENT ON COLUMN symphonie.t_borne_bor.bor_id IS
	'Séquence gérée par la base de donnée.';
	COMMENT ON COLUMN symphonie.t_borne_bor.bor_disp_id IS
	'Identifiant d''un dispositif rattaché à une borne.';
	COMMENT ON COLUMN symphonie.t_borne_bor.bor_com_id IS
	'Identifiant de la commune rattachée à une borne.';
	COMMENT ON COLUMN symphonie.t_borne_bor.bor_refg_id IS
	'Identifiant du référentiel géographique rattaché à la borne.';
	COMMENT ON COLUMN symphonie.t_borne_bor.bor_longitude IS
	'Longitude de la borne.';
	COMMENT ON COLUMN symphonie.t_borne_bor.bor_latitude IS
	'Latitude de la borne.';
	COMMENT ON COLUMN symphonie.t_borne_bor.bor_altitude IS
	'Altitude de la borne.';
	COMMENT ON COLUMN symphonie.t_borne_bor.bor_localisation IS
	'Position de la borne au sein du dispositif.';
	COMMENT ON CONSTRAINT c_fk_disp_bor ON symphonie.t_borne_bor IS
	'Rattachement d''un dispositif à une borne.';
	COMMENT ON CONSTRAINT c_fk_com_bor ON symphonie.t_borne_bor IS
	'Rattachement d''une commune à une borne.';
	COMMENT ON CONSTRAINT c_fk_refg_bor ON symphonie.t_borne_bor IS
	'Rattachement d''un référentiel géographique à une borne.';
	ALTER TABLE symphonie.t_borne_bor OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_bor_disp_id_bor
	CREATE INDEX x_btr_bor_disp_id_bor ON symphonie.t_borne_bor USING BTREE (bor_disp_id);
	COMMENT ON INDEX symphonie.x_btr_bor_disp_id_bor IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_bor_com_id_bor
	CREATE INDEX x_btr_bor_com_id_bor ON symphonie.t_borne_bor USING BTREE (bor_com_id);
	COMMENT ON INDEX symphonie.x_btr_bor_com_id_bor IS
	'Index sur le rattachement a la commune afin d''optimiser le temps des requètes';

	-- création de l'Index : x_btr_bor_refg_id_bor
	CREATE INDEX x_btr_bor_refg_id_bor ON symphonie.t_borne_bor USING BTREE (bor_refg_id);
	COMMENT ON INDEX symphonie.x_btr_bor_refg_id_bor IS
	'Index sur le rattachement au référentiel géographique afin d''optimiser le temps des requètes';

	-- création de la colonne contenant l''objet géométrique
	SELECT addgeometrycolumn('symphonie','t_borne_bor', 'geom', 2154, 'POINT', 2, false);

-- ******************************************
-- **** création des tables de jointures ****
-- ******************************************

	-- TABLE : a genotype
	CREATE TABLE symphonie.tj_a_genotype_gen_gen_agen (
		agen_gen_id				INT4			NOT NULL,
		agen_idgeno				INT4			NOT NULL,
		agen_genre_code			VARCHAR(2)		NOT NULL,
		agen_espece_code 		VARCHAR(3)		NOT NULL,
		agen_lettre_type 		CHAR			NOT NULL,
		agen_nom_court			VARCHAR(30)		NOT NULL,
		--CONSTRAINT tj_a_genotype_elem_gen_agen_pkey PRIMARY KEY (agen_elem_nom_court,agen_genre_code,agen_espece_code,agen_lettre_type,agen_nom_court),
		CONSTRAINT tj_a_genotype_gen_gen_agen_pkey PRIMARY KEY (agen_gen_id),
		CONSTRAINT c_fk_gen_agen FOREIGN KEY (agen_gen_id)
			REFERENCES symphonie.t_genotype_gen(gen_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE/*, 
		CONSTRAINT c_fk_genr_agen FOREIGN KEY (agen_genre_code)
			REFERENCES symphonie.vf_genre_vgenr(code)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE, 
		CONSTRAINT c_fk_esp_agen FOREIGN KEY (agen_espece_code)
			REFERENCES symphonie.tf_venik_espece_esp(code)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE, 
		CONSTRAINT c_fk_gen_lettre_type_agen FOREIGN KEY (agen_lettre_type)
			REFERENCES symphonie.tf_venik_genotype_gen(lettre_type)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE, 
		CONSTRAINT c_fk_gen_nom_court_agen FOREIGN KEY (agen_nom_court)
			REFERENCES symphonie.tf_venik_genotype_gen(nom_court)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE*/
	);
	COMMENT ON TABLE symphonie.tj_a_genotype_gen_gen_agen IS
	'Table contenant toutes les Associations entre un élément et sON génotype.';
	COMMENT ON COLUMN symphonie.tj_a_genotype_gen_gen_agen.agen_gen_id IS
	'Rattachement d''un génotype.';
	COMMENT ON COLUMN symphonie.tj_a_genotype_gen_gen_agen.agen_genre_code IS
	'Code du genre du génotype.';
	COMMENT ON COLUMN symphonie.tj_a_genotype_gen_gen_agen.agen_espece_code IS
	'Code de l''espèce du génotype.';
	COMMENT ON COLUMN symphonie.tj_a_genotype_gen_gen_agen.agen_lettre_type IS
	'';
	COMMENT ON COLUMN symphonie.tj_a_genotype_gen_gen_agen.agen_nom_court IS
	'Nom court du génotype.';
	COMMENT ON CONSTRAINT c_fk_gen_agen ON symphonie.tj_a_genotype_gen_gen_agen IS
	'Rattachement du génotype.';
	/*COMMENT ON CONSTRAINT c_fk_genr_agen ON symphonie.tj_a_genotype_elem_gen_agen IS
	'Rattachement de l''élément (individu) à un genre.';
	COMMENT ON CONSTRAINT c_fk_esp_agen ON symphonie.tj_a_genotype_elem_gen_agen IS
	'Rattachement de l''élément (individu) à une espèce.';
	COMMENT ON CONSTRAINT c_fk_gen_lettre_type_agen ON symphonie.tj_a_genotype_elem_gen_agen IS
	'Rattachement de l''élément (individu) à la lettre type d''un génotype.';
	COMMENT ON CONSTRAINT c_fk_gen_nom_court_agen ON symphonie.tj_a_genotype_elem_gen_agen IS
	'Rattachement de l''élément (individu) au nom court du génotype.';*/
	ALTER TABLE symphonie.tj_a_genotype_gen_gen_agen OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_agen_gen_id_agen
	CREATE INDEX x_btr_agen_gen_id_agen ON symphonie.tj_a_genotype_gen_gen_agen USING BTREE (agen_gen_id);
	COMMENT ON INDEX symphonie.x_btr_agen_gen_id_agen IS
	'Index sur le rattachement au nom court de l''élément afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_agen_idgeno_agen
	CREATE INDEX x_btr_agen_idgeno_agen ON symphonie.tj_a_genotype_gen_gen_agen USING BTREE (agen_idgeno);
	COMMENT ON INDEX symphonie.x_btr_agen_idgeno_agen IS
	'Index sur le rattachement à l''identifiant postgres du génotype afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_agen_genre_code_agen
	CREATE INDEX x_btr_agen_genre_code_agen ON symphonie.tj_a_genotype_gen_gen_agen USING BTREE (agen_genre_code);
	COMMENT ON INDEX symphonie.x_btr_agen_genre_code_agen IS
	'Index sur le rattachement au code du genre afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_agen_espece_code_agen
	CREATE INDEX x_btr_agen_espece_code_agen ON symphonie.tj_a_genotype_gen_gen_agen USING BTREE (agen_espece_code);
	COMMENT ON INDEX symphonie.x_btr_agen_espece_code_agen IS
	'Index sur le rattachement au ciode de l''espèce afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_agen_lettre_type_agen
	CREATE INDEX x_btr_agen_lettre_type_agen ON symphonie.tj_a_genotype_gen_gen_agen USING BTREE (agen_lettre_type);
	COMMENT ON INDEX symphonie.x_btr_agen_lettre_type_agen IS
	'Index sur le rattachement a la lettre type afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_agen_nom_court_agen
	CREATE INDEX x_btr_agen_nom_court_agen ON symphonie.tj_a_genotype_gen_gen_agen USING BTREE (agen_nom_court);
	COMMENT ON INDEX symphonie.x_btr_agen_nom_court_agen IS
	'Index sur le rattachement au nom court du génotype afin d''optimiser le temps des requètes';
	
	-- TABLE : a fourni
	CREATE TABLE symphonie.tj_a_fourni_elem_sam_afou (
		afou_elem_id			INT4			NOT NULL,
		afou_sample_barcode		VARCHAR(15)		NOT NULL,
		CONSTRAINT tj_a_fourni_elem_sam_afou_pkey PRIMARY KEY (afou_elem_id,afou_sample_barcode),
		CONSTRAINT c_fk_elem_afou FOREIGN KEY (afou_elem_id)
			REFERENCES symphonie.t_element_elem(elem_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE/*, 
		CONSTRAINT c_fk_barc_agen FOREIGN KEY (afou_sample_barcode)
			REFERENCES symphonie.tf_samples_barcode_barc(barcode)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE*/
	);
	COMMENT ON TABLE symphonie.tj_a_fourni_elem_sam_afou IS
	'Table contenant  toutes les Associations entre un élément (individu) et ses échantillons.';
	COMMENT ON COLUMN symphonie.tj_a_fourni_elem_sam_afou.afou_elem_id IS
	'Nom court de l''élément (individu) concerné.';
	COMMENT ON COLUMN symphonie.tj_a_fourni_elem_sam_afou.afou_sample_barcode IS
	'Code barre de l''échantillon associé à l''élément (individu).';
	COMMENT ON CONSTRAINT c_fk_elem_afou ON symphonie.tj_a_fourni_elem_sam_afou IS
	'Rattachement d''un élément (individu) à un code barre.';
	/*COMMENT ON CONSTRAINT c_fk_barc_agen ON symphonie.tj_a_fourni_elem_sam_afou IS
	'Rattachement d''un code barre à un élément (individu).';*/
	ALTER TABLE symphonie.tj_a_fourni_elem_sam_afou OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_afou_elem_nom_court_afou
	CREATE INDEX x_btr_afou_elem_id_afou ON symphonie.tj_a_fourni_elem_sam_afou USING BTREE (afou_elem_id);
	COMMENT ON INDEX symphonie.x_btr_afou_elem_id_afou IS
	'Index sur le rattachement au nom court de l''élément afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_afou_sample_barcode_afou
	CREATE INDEX x_btr_afou_sample_barcode_afou ON symphonie.tj_a_fourni_elem_sam_afou USING BTREE (afou_sample_barcode);
	COMMENT ON INDEX symphonie.x_btr_afou_sample_barcode_afou IS
	'Index sur le rattachement au code barre de l''échantillon afin d''optimiser le temps des requètes';

	-- TABLE : contient materiel
	CREATE TABLE symphonie.tj_contient_materiel_disp_mat_contm (
		contm_disp_id			INT4		NOT NULL,
		contm_mat_id			INT4		NOT NULL,
		contm_commentaire		TEXT		NULL,
		CONSTRAINT tj_contient_materiel_disp_mat_contm_pkey PRIMARY KEY (contm_disp_id,contm_mat_id),
		CONSTRAINT c_fk_disp_contm FOREIGN KEY (contm_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_mat_contm FOREIGN KEY (contm_mat_id)
			REFERENCES symphonie.t_type_materiel_mat(mat_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_contient_materiel_disp_mat_contm IS
	'Table associant le materiel végétal au dispositif.';
	COMMENT ON COLUMN symphonie.tj_contient_materiel_disp_mat_contm.contm_disp_id IS
	'Association d''un dispositif à un matériel végétal.';
	COMMENT ON COLUMN symphonie.tj_contient_materiel_disp_mat_contm.contm_mat_id IS
	'Association d''un matériel végétal à un dispositif.';
	COMMENT ON COLUMN symphonie.tj_contient_materiel_disp_mat_contm.contm_commentaire IS
	'Commentaire concernant le type de matériel végétal contenu.';
	COMMENT ON CONSTRAINT c_fk_disp_contm ON symphonie.tj_contient_materiel_disp_mat_contm IS
	'Rattachement d''un dispositif à un materiel végétal.';
	COMMENT ON CONSTRAINT c_fk_mat_contm ON symphonie.tj_contient_materiel_disp_mat_contm IS
	'Rattachement d''un materiel végétal à un dispositif.';
	ALTER TABLE symphonie.tj_contient_materiel_disp_mat_contm OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_contm_disp_id_contm
	CREATE INDEX x_btr_contm_disp_id_contm ON symphonie.tj_contient_materiel_disp_mat_contm USING BTREE (contm_disp_id);
	COMMENT ON INDEX symphonie.x_btr_contm_disp_id_contm IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_contm_mat_id_contm
	CREATE INDEX x_btr_contm_mat_id_contm ON symphonie.tj_contient_materiel_disp_mat_contm USING BTREE (contm_mat_id);
	COMMENT ON INDEX symphonie.x_btr_contm_mat_id_contm IS
	'Index sur le rattachement au matériel végétal afin d''optimiser le temps des requètes';

	-- TABLE : appartient a infrastructure
	CREATE TABLE symphonie.tj_appartient_infrastructure_gen_infra_geninf (
		geninf_gen_id		INT4		NOT NULL,
		geninf_infra_id		INT4		NOT NULL,
		CONSTRAINT tj_appartient_infrastructure_gen_infra_geninf_pkey PRIMARY KEY (geninf_gen_id,geninf_infra_id),
		CONSTRAINT c_fk_gen_geninf FOREIGN KEY (geninf_gen_id)
			REFERENCES symphonie.t_genotype_gen(gen_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_infra_geninf FOREIGN KEY (geninf_infra_id)
			REFERENCES symphonie.t_infrastructure_infra(infra_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_appartient_infrastructure_gen_infra_geninf IS
	'Table associant un génotype à une infrastructure.';
	COMMENT ON COLUMN symphonie.tj_appartient_infrastructure_gen_infra_geninf.geninf_gen_id IS
	'Association d''un génotype à une infrastructure.';
	COMMENT ON COLUMN symphonie.tj_appartient_infrastructure_gen_infra_geninf.geninf_infra_id IS
	'Association d''une infrastructure à un génotype.';
	COMMENT ON CONSTRAINT c_fk_gen_geninf ON symphonie.tj_appartient_infrastructure_gen_infra_geninf IS
	'Rattachement d''un génotype à une infrastructure.';
	COMMENT ON CONSTRAINT c_fk_infra_geninf ON symphonie.tj_appartient_infrastructure_gen_infra_geninf IS
	'Rattachement d''une infrastructure à un génotype.';
	ALTER TABLE symphonie.tj_appartient_infrastructure_gen_infra_geninf OWNER TO :proprietaire;

	-- TABLE : a description ecologique
	CREATE TABLE symphonie.tj_a_description_eco_disp_ecod_aecod (
		aecod_disp_id		INT4		NOT NULL,
		aecod_ecod_id 		INT4		NOT NULL,
		CONSTRAINT tj_a_description_eco_disp_ecod_aecod_pkey PRIMARY KEY (aecod_disp_id,aecod_ecod_id),
		CONSTRAINT c_fk_disp_aecod FOREIGN KEY (aecod_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_ecod_aecod FOREIGN KEY (aecod_ecod_id)
			REFERENCES symphonie.t_description_ecologique_ecod(ecod_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_description_eco_disp_ecod_aecod IS
	'Table associant les descriptions écologiques qu dispositif.';
	COMMENT ON COLUMN symphonie.tj_a_description_eco_disp_ecod_aecod.aecod_disp_id IS
	'Association d''un dispositif à une descrption écologique.';
	COMMENT ON COLUMN symphonie.tj_a_description_eco_disp_ecod_aecod.aecod_ecod_id IS
	'Association d''une description écologique à un dispositif.';
	COMMENT ON CONSTRAINT c_fk_disp_aecod ON symphonie.tj_a_description_eco_disp_ecod_aecod IS
	'Rattachement d''un dispositif à une description écologique.';
	COMMENT ON CONSTRAINT c_fk_ecod_aecod ON symphonie.tj_a_description_eco_disp_ecod_aecod IS
	'Rattachement d''une description écologique à un dispositif.';
	ALTER TABLE symphonie.tj_a_description_eco_disp_ecod_aecod OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_aecod_disp_id_aecod
	CREATE INDEX x_btr_aecod_disp_id_aecod ON symphonie.tj_a_description_eco_disp_ecod_aecod USING BTREE (aecod_disp_id);
	COMMENT ON INDEX symphonie.x_btr_aecod_disp_id_aecod IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_aecod_ecod_id_aecod
	CREATE INDEX x_btr_aecod_ecod_id_aecod ON symphonie.tj_a_description_eco_disp_ecod_aecod USING BTREE (aecod_ecod_id);
	COMMENT ON INDEX symphonie.x_btr_aecod_ecod_id_aecod IS
	'Index sur le rattachement a la description écologique afin d''optimiser le temps des requètes';

	-- TABLE : a semis
	CREATE TABLE symphonie.tj_a_semis_disp_sem_asem (
		asem_disp_id        INT4		NOT NULL,
		asem_sem_id 		INT4		NOT NULL,
		CONSTRAINT tj_a_semis_disp_sem_asem_pkey PRIMARY KEY (asem_disp_id,asem_sem_id),
		CONSTRAINT c_fk_disp_asem FOREIGN KEY (asem_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_sem_asem FOREIGN KEY (asem_sem_id)
			REFERENCES symphonie.t_semis_sem(sem_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_semis_disp_sem_asem IS
	'Table associant les semis à un dispositif.';
	COMMENT ON COLUMN symphonie.tj_a_semis_disp_sem_asem.asem_disp_id IS
	'Association d''un dispositif avec un semis.';
	COMMENT ON COLUMN symphonie.tj_a_semis_disp_sem_asem.asem_sem_id IS
	'Association d''un semis avec un dispositif.';
	COMMENT ON CONSTRAINT c_fk_disp_asem ON symphonie.tj_a_semis_disp_sem_asem IS
	'Rattachement d''un dispositif à un semis.';
	COMMENT ON CONSTRAINT c_fk_sem_asem ON symphonie.tj_a_semis_disp_sem_asem IS
	'Rattachement d''un semis à un dispositif.';
	ALTER TABLE symphonie.tj_a_semis_disp_sem_asem OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_asem_disp_id_asem
	CREATE INDEX x_btr_asem_disp_id_asem ON symphonie.tj_a_semis_disp_sem_asem USING BTREE (asem_disp_id);
	COMMENT ON INDEX symphonie.x_btr_asem_disp_id_asem IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_asem_sem_id_asem
	CREATE INDEX x_btr_asem_sem_id_asem ON symphonie.tj_a_semis_disp_sem_asem USING BTREE (asem_sem_id);
	COMMENT ON INDEX symphonie.x_btr_asem_sem_id_asem IS
	'Index sur le rattachement au semis afin d''optimiser le temps des requètes';
	
	-- TABLE : a comme antecedent
	CREATE TABLE symphonie.tj_a_antecedent_disp_ant_antd (
		antd_disp_id        INT4		NOT NULL,
		antd_ant_id 		INT4		NOT NULL,
		CONSTRAINT tj_a_antecedent_disp_ant_antd_pkey PRIMARY KEY (antd_disp_id,antd_ant_id),
		CONSTRAINT c_fk_disp_antd FOREIGN KEY (antd_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_ant_antd FOREIGN KEY (antd_ant_id)
			REFERENCES symphonie.t_antecedent_ant(ant_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_antecedent_disp_ant_antd IS
	'Table associant des antécédents à un dispositif.';
	COMMENT ON COLUMN symphonie.tj_a_antecedent_disp_ant_antd.antd_disp_id IS
	'Association du dispositif avec un antécédent.';
	COMMENT ON COLUMN symphonie.tj_a_antecedent_disp_ant_antd.antd_ant_id IS
	'Association d''un antécédent avec un dispositif.';
	COMMENT ON CONSTRAINT c_fk_disp_antd ON symphonie.tj_a_antecedent_disp_ant_antd IS
	'Rattachement d''un dispositif à un antécédent.';
	COMMENT ON CONSTRAINT c_fk_ant_antd ON symphonie.tj_a_antecedent_disp_ant_antd IS
	'Rattachement d''un antécédent à un dispositif.';
	ALTER TABLE symphonie.tj_a_antecedent_disp_ant_antd OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_antd_ant_id_antd
	CREATE INDEX x_btr_antd_disp_id_antd ON symphonie.tj_a_antecedent_disp_ant_antd USING BTREE (antd_disp_id);
	COMMENT ON INDEX symphonie.x_btr_antd_disp_id_antd IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_antd_disp_id_antd
	CREATE INDEX x_btr_antd_ant_id_antd ON symphonie.tj_a_antecedent_disp_ant_antd USING BTREE (antd_ant_id);
	COMMENT ON INDEX symphonie.x_btr_antd_ant_id_antd IS
	'Index sur le rattachement a l''antécédent afin d''optimiser le temps des requètes';
	
	-- TABLE : especes presentent
	CREATE TABLE symphonie.tj_espece_presente_disp_esp_espp (
		espp_disp_id        INT4		NOT NULL,
		espp_esp_id 		INT4		NOT NULL,
		CONSTRAINT tj_espece_presente_disp_esp_espp_pkey PRIMARY KEY (espp_disp_id,espp_esp_id),
		CONSTRAINT c_fk_disp_espp FOREIGN KEY (espp_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_esp_espp FOREIGN KEY (espp_esp_id)
			REFERENCES symphonie.tr_espece_esp(esp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_espece_presente_disp_esp_espp IS
	'Table associant les espèces présentent dans le dispositif.';
	COMMENT ON COLUMN symphonie.tj_espece_presente_disp_esp_espp.espp_disp_id IS
	'Association d''un dispositif et d''une espèce.';
	COMMENT ON COLUMN symphonie.tj_espece_presente_disp_esp_espp.espp_esp_id IS
	'Association d''une espèce avec un dispositif.';
	COMMENT ON CONSTRAINT c_fk_disp_espp ON symphonie.tj_espece_presente_disp_esp_espp IS
	'Rattachement d''un dispositif à une espèce.';
	COMMENT ON CONSTRAINT c_fk_esp_espp ON symphonie.tj_espece_presente_disp_esp_espp IS
	'Rattachement d''une espèce à un dispositif.';
	ALTER TABLE symphonie.tj_espece_presente_disp_esp_espp OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_espp_disp_id_espp
	CREATE INDEX x_btr_espp_disp_id_espp ON symphonie.tj_espece_presente_disp_esp_espp USING BTREE (espp_disp_id);
	COMMENT ON INDEX symphonie.x_btr_espp_disp_id_espp IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_espp_esp_id_espp
	CREATE INDEX x_btr_espp_esp_id_espp ON symphonie.tj_espece_presente_disp_esp_espp USING BTREE (espp_esp_id);
	COMMENT ON INDEX symphonie.x_btr_espp_esp_id_espp IS
	'Index sur le rattachement a l''espèce afin d''optimiser le temps des requètes';
	
	-- TABLE : a convention
	CREATE TABLE symphonie.tj_a_convention_disp_conv_aconv (
		aconv_disp_id		INT4		NOT NULL,
		aconv_conv_id 		INT4		NOT NULL,
		CONSTRAINT tj_a_convention_disp_conv_aconv_pkey PRIMARY KEY (aconv_disp_id,aconv_conv_id),
		CONSTRAINT c_fk_disp_aconv FOREIGN KEY (aconv_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_conv_aconv FOREIGN KEY (aconv_conv_id)
			REFERENCES symphonie.t_convention_conv(conv_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_convention_disp_conv_aconv IS
	'Table associant les conventions au dispositif.';
	COMMENT ON COLUMN symphonie.tj_a_convention_disp_conv_aconv.aconv_disp_id IS
	'Association d''un dispositif à une convention.';
	COMMENT ON COLUMN symphonie.tj_a_convention_disp_conv_aconv.aconv_conv_id IS
	'Association d''une convetion à un dispositif.';
	COMMENT ON CONSTRAINT c_fk_disp_aconv ON symphonie.tj_a_convention_disp_conv_aconv IS
	'Rattachement d''un dispositif à une convetion.';
	COMMENT ON CONSTRAINT c_fk_conv_aconv ON symphonie.tj_a_convention_disp_conv_aconv IS
	'Rattachement d''une convention à un dispositif.';
	ALTER TABLE symphonie.tj_a_convention_disp_conv_aconv OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_aconv_disp_id_aconv
	CREATE INDEX x_btr_aconv_disp_id_aconv ON symphonie.tj_a_convention_disp_conv_aconv USING BTREE (aconv_disp_id);
	COMMENT ON INDEX symphonie.x_btr_aconv_disp_id_aconv IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_aconv_conv_id_aconv
	CREATE INDEX x_btr_aconv_conv_id_aconv ON symphonie.tj_a_convention_disp_conv_aconv USING BTREE (aconv_conv_id);
	COMMENT ON INDEX symphonie.x_btr_aconv_conv_id_aconv IS
	'Index sur le rattachement a la convention afin d''optimiser le temps des requètes';
	
	-- TABLE : preparation racine utilisee
	CREATE TABLE symphonie.tj_prepr_utilisee_disp_prepr_prepru (
		prepru_disp_id			INT4		NOT NULL,
		prepru_prepr_id 		INT4		NOT NULL,
		CONSTRAINT tj_prepr_utilisee_disp_prepr_prepru_pkey PRIMARY KEY (prepru_disp_id,prepru_prepr_id),
		CONSTRAINT c_fk_disp_prepru FOREIGN KEY (prepru_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_prepr_prepru FOREIGN KEY (prepru_prepr_id)
			REFERENCES symphonie.t_preparation_racine_prepr(prepr_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_prepr_utilisee_disp_prepr_prepru IS
	'Table associant les préprations de racines au dispositif.';
	COMMENT ON COLUMN symphonie.tj_prepr_utilisee_disp_prepr_prepru.prepru_disp_id IS
	'Association d''un dispositif à une préparation de racine.';
	COMMENT ON COLUMN symphonie.tj_prepr_utilisee_disp_prepr_prepru.prepru_prepr_id IS
	'Association d''une préparation de racine à un dispositif.';
	COMMENT ON CONSTRAINT c_fk_disp_prepru ON symphonie.tj_prepr_utilisee_disp_prepr_prepru IS
	'Rattachement d''un dispositif à une préparation de racine.';
	COMMENT ON CONSTRAINT c_fk_prepr_prepru ON symphonie.tj_prepr_utilisee_disp_prepr_prepru IS
	'Rattachement d''une préparation de racine à un dispositif.';
	ALTER TABLE symphonie.tj_prepr_utilisee_disp_prepr_prepru OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_prepru_disp_id_prepru
	CREATE INDEX x_btr_prepru_disp_id_prepru ON symphonie.tj_prepr_utilisee_disp_prepr_prepru USING BTREE (prepru_disp_id);
	COMMENT ON INDEX symphonie.x_btr_prepru_disp_id_prepru IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_prepru_prepr_id_prepru
	CREATE INDEX x_btr_prepru_prepr_id_prepru ON symphonie.tj_prepr_utilisee_disp_prepr_prepru USING BTREE (prepru_prepr_id);
	COMMENT ON INDEX symphonie.x_btr_prepru_prepr_id_prepru IS
	'Index sur le rattachement a la préparation de racine utilisée afin d''optimiser le temps des requètes';
	
	-- TABLE : mode plantation utilise
	CREATE TABLE symphonie.tj_modp_utilise_disp_modp_modpu (
		modpu_disp_id		INT4		NOT NULL,
		modpu_modp_id 		INT4		NOT NULL,
		CONSTRAINT tj_modp_utilise_disp_modp_modpu_pkey PRIMARY KEY (modpu_disp_id,modpu_modp_id),
		CONSTRAINT c_fk_disp_modpu FOREIGN KEY (modpu_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_modp_modpu FOREIGN KEY (modpu_modp_id)
			REFERENCES symphonie.t_mode_plantation_modp(modp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_modp_utilise_disp_modp_modpu IS
	'Table associant les modes de plantations au dispositif.';
	COMMENT ON COLUMN symphonie.tj_modp_utilise_disp_modp_modpu.modpu_disp_id IS
	'Association d''un dispositif à un mode de plantation.';
	COMMENT ON COLUMN symphonie.tj_modp_utilise_disp_modp_modpu.modpu_modp_id IS
	'Association d''un mode de plantation à un dispositif.';
	COMMENT ON CONSTRAINT c_fk_disp_modpu ON symphonie.tj_modp_utilise_disp_modp_modpu IS
	'Rattachement d''un dispositif à un mode de plantation.';
	COMMENT ON CONSTRAINT c_fk_modp_modpu ON symphonie.tj_modp_utilise_disp_modp_modpu IS
	'Rattachement d''un mode de plantation à un dispositif.';
	ALTER TABLE symphonie.tj_modp_utilise_disp_modp_modpu OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_modpu_disp_id_modpu
	CREATE INDEX x_btr_modpu_disp_id_modpu ON symphonie.tj_modp_utilise_disp_modp_modpu USING BTREE (modpu_disp_id);
	COMMENT ON INDEX symphonie.x_btr_modpu_disp_id_modpu IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_modpu_modp_id_modpu
	CREATE INDEX x_btr_modpu_modp_id_modpu ON symphonie.tj_modp_utilise_disp_modp_modpu USING BTREE (modpu_modp_id);
	COMMENT ON INDEX symphonie.x_btr_modpu_modp_id_modpu IS
	'Index sur le rattachement au mode de plantation utilisé afin d''optimiser le temps des requètes';
	
	-- TABLE : appartient regroupement
	CREATE TABLE symphonie.tj_appartient_regroupement_disp_rgpd_apprgpd (
		apprgpd_disp_id			INT4		NOT NULL,
		apprgpd_rgpd_id 		INT4		NOT NULL,
		apprgpd_debut           DATE		NULL,
		apprgpd_fin             DATE		NULL,
		apprgpd_ordre           INT4		NULL,
		CONSTRAINT tj_appartient_regroupement_disp_grpd_apprgpd_pkey PRIMARY KEY (apprgpd_disp_id,apprgpd_rgpd_id),
		CONSTRAINT c_fk_disp_apprgpd FOREIGN KEY (apprgpd_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_rgpd_apprgpd FOREIGN KEY (apprgpd_rgpd_id)
			REFERENCES symphonie.t_regroupement_dispositif_rgpd(rgpd_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_appartient_regroupement_disp_rgpd_apprgpd IS
	'Table associant les espèces présentent dans le dispositif.';
	COMMENT ON COLUMN symphonie.tj_appartient_regroupement_disp_rgpd_apprgpd.apprgpd_disp_id IS
	'Dispositif appartenant aux regroupement.';
	COMMENT ON COLUMN symphonie.tj_appartient_regroupement_disp_rgpd_apprgpd.apprgpd_rgpd_id IS
	'Regroupement de dispositif concerné.';
	COMMENT ON COLUMN symphonie.tj_appartient_regroupement_disp_rgpd_apprgpd.apprgpd_debut IS
	'Date à laquelle le dispositif à commencer a appartenir au regroupement.';
	COMMENT ON COLUMN symphonie.tj_appartient_regroupement_disp_rgpd_apprgpd.apprgpd_fin IS
	'Date à laquelle le dispositif à cesser d''appartenir au regroupement.';
	COMMENT ON COLUMN symphonie.tj_appartient_regroupement_disp_rgpd_apprgpd.apprgpd_ordre IS
	'Ordre du dispositif à l''interieur du regroupement.';
	COMMENT ON CONSTRAINT c_fk_disp_apprgpd ON symphonie.tj_appartient_regroupement_disp_rgpd_apprgpd IS
	'Rattachement d''un dispositif à un regroupement de dispositif.';
	COMMENT ON CONSTRAINT c_fk_rgpd_apprgpd ON symphonie.tj_appartient_regroupement_disp_rgpd_apprgpd IS
	'Rattachement d''un regroupement de dispositif à un dispositif.';
	ALTER TABLE symphonie.tj_appartient_regroupement_disp_rgpd_apprgpd OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_apprgpd_disp_id_apprgpd
	CREATE INDEX x_btr_apprgpd_disp_id_apprgpd ON symphonie.tj_appartient_regroupement_disp_rgpd_apprgpd USING BTREE (apprgpd_disp_id);
	COMMENT ON INDEX symphonie.x_btr_apprgpd_disp_id_apprgpd IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_apprgpd_rgpd_id_apprgpd
	CREATE INDEX x_btr_apprgpd_rgpd_id_apprgpd ON symphonie.tj_appartient_regroupement_disp_rgpd_apprgpd USING BTREE (apprgpd_rgpd_id);
	COMMENT ON INDEX symphonie.x_btr_apprgpd_rgpd_id_apprgpd IS
	'Index sur le rattachement au regroupement de dispositifs afin d''optimiser le temps des requètes';
	
	-- TABLE : a localisation
	CREATE TABLE symphonie.tj_a_localisation_disp_pays_aloc (
		aloc_disp_id			INT4				NOT NULL,
		aloc_pays_id			INT4				NOT NULL,
		aloc_localisation_1		VARCHAR(100)		NULL,
		aloc_localisation_2		VARCHAR(100)		NULL,
		aloc_localisation_3		VARCHAR(100)		NULL,
		CONSTRAINT tj_a_localisation_disp_pays_aloc_pkey PRIMARY KEY (aloc_disp_id,aloc_pays_id),
		CONSTRAINT c_fk_disp_aloc FOREIGN KEY (aloc_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_pays_aloc FOREIGN KEY (aloc_pays_id)
			REFERENCES symphonie.tr_pays_pays(pays_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_localisation_disp_pays_aloc IS
	'Table contenant les différents éléments de localisation d''un dispositif dans un pays étrangé.';
	COMMENT ON COLUMN symphonie.tj_a_localisation_disp_pays_aloc.aloc_disp_id IS
	'Identifiant du dispositif associé à la localisation.';
	COMMENT ON COLUMN symphonie.tj_a_localisation_disp_pays_aloc.aloc_pays_id IS
	'Identifiant du pays rattaché à la localisation.';
	COMMENT ON COLUMN symphonie.tj_a_localisation_disp_pays_aloc.aloc_localisation_1 IS
	'Premier élément de localisation d''un dispositif dans un pays étrangé.';
	COMMENT ON COLUMN symphonie.tj_a_localisation_disp_pays_aloc.aloc_localisation_2 IS
	'Deuxème élément de localisation d''un dispositf dans un pays étrangé.';
	COMMENT ON COLUMN symphonie.tj_a_localisation_disp_pays_aloc.aloc_localisation_3 IS
	'Troisième élément de localisation d''un dispositif dans un pays étrangé.';
	COMMENT ON CONSTRAINT c_fk_disp_aloc ON symphonie.tj_a_localisation_disp_pays_aloc IS
	'Rattachement d''un dispositif à un pays.';
	COMMENT ON CONSTRAINT c_fk_pays_aloc ON symphonie.tj_a_localisation_disp_pays_aloc IS
	'Rattachement d''un pays à un dispositif.';
	ALTER TABLE symphonie.tj_a_localisation_disp_pays_aloc OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_aloc_disp_id_aloc
	CREATE INDEX x_btr_aloc_disp_id_aloc ON symphonie.tj_a_localisation_disp_pays_aloc USING BTREE (aloc_disp_id);
	COMMENT ON INDEX symphonie.x_btr_aloc_disp_id_aloc IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_aloc_pays_id_aloc
	CREATE INDEX x_btr_aloc_pays_id_aloc ON symphonie.tj_a_localisation_disp_pays_aloc USING BTREE (aloc_pays_id);
	COMMENT ON INDEX symphonie.x_btr_aloc_pays_id_aloc IS
	'Index sur le rattachement au pays afin d''optimiser le temps des requètes';
	
	-- TABLE : localisation
	CREATE TABLE symphonie.tj_localisation_disp_com_loc (
		loc_disp_id			INT4				NOT NULL,
		loc_com_id			INT4				NOT NULL,
		loc_lieu_dit		VARCHAR(255)		NULL,
		CONSTRAINT tj_localisation_disp_com_loc_pkey PRIMARY KEY (loc_disp_id,loc_com_id),
		CONSTRAINT c_fk_disp_loc FOREIGN KEY (loc_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_com_loc FOREIGN KEY (loc_com_id)
			REFERENCES symphonie.tr_communes_com(com_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_localisation_disp_com_loc IS
	'Table contenant les différents éléments de localisation d''un dispositif dans un pays étrangé.';
	COMMENT ON COLUMN symphonie.tj_localisation_disp_com_loc.loc_disp_id IS
	'Identifiant du dispositif associé à la commune.';
	COMMENT ON COLUMN symphonie.tj_localisation_disp_com_loc.loc_com_id IS
	'Identifiant de la commune associée au dispositif.';
	COMMENT ON COLUMN symphonie.tj_localisation_disp_com_loc.loc_lieu_dit IS
	'Nom du lieu dit de la localisation.';
	COMMENT ON CONSTRAINT c_fk_disp_loc ON symphonie.tj_localisation_disp_com_loc IS
	'Rattachement d''un dispositif à une commune.';
	COMMENT ON CONSTRAINT c_fk_com_loc ON symphonie.tj_localisation_disp_com_loc IS
	'Rattachement d''une commune à un dispositif.';
	ALTER TABLE symphonie.tj_localisation_disp_com_loc OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_loc_disp_id_loc
	CREATE INDEX x_btr_loc_disp_id_loc ON symphonie.tj_localisation_disp_com_loc USING BTREE (loc_disp_id);
	COMMENT ON INDEX symphonie.x_btr_loc_disp_id_loc IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_loc_com_id_loc
	CREATE INDEX x_btr_loc_com_id_loc ON symphonie.tj_localisation_disp_com_loc USING BTREE (loc_com_id);
	COMMENT ON INDEX symphonie.x_btr_loc_com_id_loc IS
	'Index sur le rattachement a la commune afin d''optimiser le temps des requètes';
	
	-- TABLE : est synonyme
	CREATE TABLE symphonie.tj_a_synonyme_gen_syn_gensyn (
		gensyn_gen_id		INT4	NOT NULL,
		gensyn_syn_id		INT4	NOT NULL,
		CONSTRAINT tj_a_synonyme_gen_syn_gensyn_pkey PRIMARY KEY (gensyn_gen_id, gensyn_syn_id),
		CONSTRAINT c_fk_gen_gensyn FOREIGN KEY (gensyn_gen_id)
			REFERENCES symphonie.t_genotype_gen(gen_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_syn_gensyn FOREIGN KEY (gensyn_syn_id)
			REFERENCES symphonie.t_synonyme_syn(syn_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_synonyme_gen_syn_gensyn IS
	'Table contenant toutes les synonymie pour les génotypes.';
	COMMENT ON COLUMN symphonie.tj_a_synonyme_gen_syn_gensyn.gensyn_gen_id IS
	'Identifiant du génotype.';
	COMMENT ON COLUMN symphonie.tj_a_synonyme_gen_syn_gensyn.gensyn_syn_id IS
	'Identifiant du nom synonyme.';
	COMMENT ON CONSTRAINT c_fk_gen_gensyn ON symphonie.tj_a_synonyme_gen_syn_gensyn IS
	'Rattachement d''un génotype à un synonyme.';
	COMMENT ON CONSTRAINT c_fk_syn_gensyn ON symphonie.tj_a_synonyme_gen_syn_gensyn IS
	'Rattachement d''un synonyme à un génotype.';
	ALTER TABLE symphonie.tj_a_synonyme_gen_syn_gensyn OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_gen_id_gensyn
	CREATE INDEX x_btr_gen_id_gensyn ON symphonie.tj_a_synonyme_gen_syn_gensyn USING BTREE (gensyn_gen_id);
	COMMENT ON INDEX symphonie.x_btr_gen_id_gensyn IS
	'Index sur le rattachement du génotype afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_syn_id_gensyn
	CREATE INDEX x_btr_syn_id_gensyn ON symphonie.tj_a_synonyme_gen_syn_gensyn USING BTREE (gensyn_syn_id);
	COMMENT ON INDEX symphonie.x_btr_syn_id_gensyn IS
	'Index sur le rattachement du nom synonyme du génotype afin d''optimiser le temps des requètes';
	
	-- TABLE : role dispositif - operateur
	CREATE TABLE symphonie.tj_a_role_disp_ope_rol_rold (
		rold_disp_id	INT4	NOT NULL,
		rold_ope_id		INT4	NOT NULL,
		rold_rol_id		INT4	NOT NULL,
		CONSTRAINT tj_a_role_disp_ope_rol_rold_pkey PRIMARY KEY (rold_disp_id, rold_ope_id, rold_rol_id),
		CONSTRAINT c_fk_disp_rold FOREIGN KEY (rold_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_ope_rold FOREIGN KEY (rold_ope_id)
			REFERENCES symphonie.t_operateur_ope(ope_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_rol_rold FOREIGN KEY (rold_rol_id)
			REFERENCES symphonie.t_role_rol(rol_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_role_disp_ope_rol_rold IS
	'Table contenant les rôles des différentes personnes (ex : gestionnaire, propriétaire, référent scientifique, etc…).';
	COMMENT ON COLUMN symphonie.tj_a_role_disp_ope_rol_rold.rold_disp_id IS
	'Association d''un dispositif à un rôle, donc à un opérateur.';
	COMMENT ON COLUMN symphonie.tj_a_role_disp_ope_rol_rold.rold_ope_id IS
	'Association d''un opérateur à un rôle.';
	COMMENT ON COLUMN symphonie.tj_a_role_disp_ope_rol_rold.rold_rol_id IS
	'Association du role à un opérateur pour un dispositif.';
	COMMENT ON CONSTRAINT c_fk_disp_rold ON symphonie.tj_a_role_disp_ope_rol_rold IS
	'Rattachement d''un dispositif à un rôle.';
	COMMENT ON CONSTRAINT c_fk_ope_rold ON symphonie.tj_a_role_disp_ope_rol_rold IS
	'Rattachement d''un opérateur à un rôle.';
	COMMENT ON CONSTRAINT c_fk_rol_rold ON symphonie.tj_a_role_disp_ope_rol_rold IS
	'Rattachement d''un role.';
	ALTER TABLE symphonie.tj_a_role_disp_ope_rol_rold OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_rold_disp_id_rold
	CREATE INDEX x_btr_rold_disp_id_rold ON symphonie.tj_a_role_disp_ope_rol_rold USING BTREE (rold_disp_id);
	COMMENT ON INDEX symphonie.x_btr_rold_disp_id_rold IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_rold_ope_id_rold
	CREATE INDEX x_btr_rold_ope_id_rold ON symphonie.tj_a_role_disp_ope_rol_rold USING BTREE (rold_ope_id);
	COMMENT ON INDEX symphonie.x_btr_rold_ope_id_rold IS
	'Index sur le rattachement a l''opérateur afin d''optimiser le temps des requètes';

	-- création de l'Index : x_btr_rold_rol_id_rold
	CREATE INDEX x_btr_rold_rol_id_rold ON symphonie.tj_a_role_disp_ope_rol_rold USING BTREE (rold_rol_id);
	COMMENT ON INDEX symphonie.x_btr_rold_rol_id_rold IS
	'Index sur le rattachement du role afin d''optimiser le temps des requètes';
	
	-- TABLE : role mesure - operateur
	CREATE TABLE symphonie.tj_a_role_mes_ope_rol_rolm (
		rolm_mes_id		INT4	NULL,
		rolm_ope_id		INT4	NULL,
		rolm_rol_id		INT4	NOT NULL,
		CONSTRAINT tj_a_role_mes_ope_rol_rolm_pkey PRIMARY KEY (rolm_mes_id, rolm_ope_id, rolm_rol_id),
		CONSTRAINT c_fk_mes_rolm FOREIGN KEY (rolm_mes_id)
			REFERENCES symphonie.t_mesure_mes(mes_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_ope_rolm FOREIGN KEY (rolm_ope_id)
			REFERENCES symphonie.t_operateur_ope(ope_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_rol_rolm FOREIGN KEY (rolm_rol_id)
			REFERENCES symphonie.t_role_rol(rol_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_role_mes_ope_rol_rolm IS
	'Table contenant les rôles des différentes personnes (ex : gestionnaire, propriétaire, référent scientifique, etc…).';
	COMMENT ON COLUMN symphonie.tj_a_role_mes_ope_rol_rolm.rolm_mes_id IS
	'Association d''une mesure à un rôle, donc à un opérateur.';
	COMMENT ON COLUMN symphonie.tj_a_role_mes_ope_rol_rolm.rolm_ope_id IS
	'Association d''un opérateur à un rôle.';
	COMMENT ON COLUMN symphonie.tj_a_role_mes_ope_rol_rolm.rolm_rol_id IS
	'Association du role à un opérateur pour un mesure.';
	COMMENT ON CONSTRAINT c_fk_mes_rolm ON symphonie.tj_a_role_mes_ope_rol_rolm IS
	'Rattachement d''une mesure à un role.';
	COMMENT ON CONSTRAINT c_fk_ope_rolm ON symphonie.tj_a_role_mes_ope_rol_rolm IS
	'Rattachement d''un opérateur à un rôle.';
	COMMENT ON CONSTRAINT c_fk_rol_rolm ON symphonie.tj_a_role_mes_ope_rol_rolm IS
	'Rattachement d''un role.';
	ALTER TABLE symphonie.tj_a_role_mes_ope_rol_rolm OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_rolm_mes_id_rolm
	CREATE INDEX x_btr_rolm_mes_id_rolm ON symphonie.tj_a_role_mes_ope_rol_rolm USING BTREE (rolm_mes_id);
	COMMENT ON INDEX symphonie.x_btr_rolm_mes_id_rolm IS
	'Index sur le rattachement a la mesure afin d''optimiser le temps des requètes';

	-- création de l'Index : x_btr_rolm_ope_id_rolm
	CREATE INDEX x_btr_rolm_ope_id_rolm ON symphonie.tj_a_role_mes_ope_rol_rolm USING BTREE (rolm_ope_id);
	COMMENT ON INDEX symphonie.x_btr_rolm_ope_id_rolm IS
	'Index sur le rattachement a l''opérateur afin d''optimiser le temps des requètes';

	-- création de l'Index : x_btr_rolm_rol_id_rolm
	CREATE INDEX x_btr_rolm_rol_id_rolm ON symphonie.tj_a_role_mes_ope_rol_rolm USING BTREE (rolm_rol_id);
	COMMENT ON INDEX symphonie.x_btr_rolm_rol_id_rolm IS
	'Index sur le rattachement du role afin d''optimiser le temps des requètes';
	
	-- TABLE : role regroupement de mesure - operateur
	CREATE TABLE symphonie.tj_a_role_rgpm_ope_rol_rolr (
		rolr_rgpm_id		INT4	NULL,
		rolr_ope_id			INT4	NULL,
		rolr_rol_id			INT4	NOT NULL,
		CONSTRAINT tj_a_role_rgpm_ope_rol_rolr_pkey PRIMARY KEY (rolr_rgpm_id, rolr_ope_id, rolr_rol_id),
		CONSTRAINT c_fk_rgpm_rolr FOREIGN KEY (rolr_rgpm_id)
			REFERENCES symphonie.t_regroupement_mesure_rgpm(rgpm_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_ope_rolr FOREIGN KEY (rolr_ope_id)
			REFERENCES symphonie.t_operateur_ope(ope_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_rol_rolr FOREIGN KEY (rolr_rol_id)
			REFERENCES symphonie.t_role_rol(rol_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_role_rgpm_ope_rol_rolr IS
	'Table contenant les rôles des différentes personnes (ex : gestionnaire, propriétaire, référent scientifique, etc…).';
	COMMENT ON COLUMN symphonie.tj_a_role_rgpm_ope_rol_rolr.rolr_rgpm_id IS
	'Association d''un regroupement de mesure à un rôle, donc à un opérateur.';
	COMMENT ON COLUMN symphonie.tj_a_role_rgpm_ope_rol_rolr.rolr_ope_id IS
	'Association d''un opérateur à un rôle.';
	COMMENT ON COLUMN symphonie.tj_a_role_rgpm_ope_rol_rolr.rolr_rol_id IS
	'Association du role à un opérateur pour un regroupement de mesure.';
	COMMENT ON CONSTRAINT c_fk_rgpm_rolr ON symphonie.tj_a_role_rgpm_ope_rol_rolr IS
	'Rattachement d''un regroupement de mesure à un rôle.';
	COMMENT ON CONSTRAINT c_fk_ope_rolr ON symphonie.tj_a_role_rgpm_ope_rol_rolr IS
	'Rattachement d''un opérateur à un rôle.';
	COMMENT ON CONSTRAINT c_fk_rol_rolr ON symphonie.tj_a_role_rgpm_ope_rol_rolr IS
	'Rattachement d''un role.';
	ALTER TABLE symphonie.tj_a_role_rgpm_ope_rol_rolr OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_rolr_rgpm_id_rolr
	CREATE INDEX x_btr_rolr_rgpm_id_rolr ON symphonie.tj_a_role_rgpm_ope_rol_rolr USING BTREE (rolr_rgpm_id);
	COMMENT ON INDEX symphonie.x_btr_rolr_rgpm_id_rolr IS
	'Index sur le rattachement au regroupement de mesures afin d''optimiser le temps des requètes';

	-- création de l'Index : x_btr_rolr_ope_id_rolr
	CREATE INDEX x_btr_rolr_ope_id_rolr ON symphonie.tj_a_role_rgpm_ope_rol_rolr USING BTREE (rolr_ope_id);
	COMMENT ON INDEX symphonie.x_btr_rolr_ope_id_rolr IS
	'Index sur le rattachement a l''opérateur afin d''optimiser le temps des requètes';

	-- création de l'Index : x_btr_rolr_rol_id_rolr
	CREATE INDEX x_btr_rolr_rol_id_rolr ON symphonie.tj_a_role_rgpm_ope_rol_rolr USING BTREE (rolr_rol_id);
	COMMENT ON INDEX symphonie.x_btr_rolr_rol_id_rolr IS
	'Index sur le rattachement du role afin d''optimiser le temps des requètes';
	
	-- TABLE : a pour metadonnee - dispositif
	CREATE TABLE symphonie.tj_a_metadonnee_disp_meta_metad (
		metad_meta_id		INT4				NULL,
		metad_disp_id		INT4				NULL,
		metad_valeur		VARCHAR(100)		NOT NULL,
		metad_url			VARCHAR(255)		NULL,
		metad_date			date				NULL,
		metad_remarque		TEXT				NULL,
		CONSTRAINT tj_a_metadonnee_disp_meta_metad_pkey PRIMARY KEY (metad_meta_id,metad_disp_id),
		CONSTRAINT c_fk_meta_metad FOREIGN KEY (metad_meta_id)
			REFERENCES symphonie.t_metadonnee_meta(meta_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_disp_metad FOREIGN KEY (metad_disp_id)
			REFERENCES symphonie.t_dispositif_disp(disp_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_metadonnee_disp_meta_metad IS
	'Table associant des métadonnée à un dispositif, à un élément (individu), à une mesure ou à un regroupement de mesures.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_disp_meta_metad.metad_meta_id IS
	'Association d''une métadonnée.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_disp_meta_metad.metad_disp_id IS
	'Association d''une métadonnée à un dispositif.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_disp_meta_metad.metad_valeur IS
	'Valeur de la métadonnée.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_disp_meta_metad.metad_url IS
	'Lien vers une page décrivant/expliquant la métadonnée.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_disp_meta_metad.metad_date IS
	'Date à laquelle la métadonnée a été associée au dispositif.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_disp_meta_metad.metad_remarque IS
	'Remarque concernant l''association de la métadonnée au dispositif.';
	COMMENT ON CONSTRAINT c_fk_meta_metad ON symphonie.tj_a_metadonnee_disp_meta_metad IS
	'Rattachement d''une métadonnée à une valeur de métadonnée.';
	COMMENT ON CONSTRAINT c_fk_disp_metad ON symphonie.tj_a_metadonnee_disp_meta_metad IS
	'Rattachement d''un dispositif à une valeur de méradonnée.';
	ALTER TABLE symphonie.tj_a_metadonnee_disp_meta_metad OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_metad_meta_id_metad
	CREATE INDEX x_btr_metad_meta_id_metad ON symphonie.tj_a_metadonnee_disp_meta_metad USING BTREE (metad_meta_id);
	COMMENT ON INDEX symphonie.x_btr_metad_meta_id_metad IS
	'Index sur le rattachement a la métadonnée afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_metad_disp_id_metad
	CREATE INDEX x_btr_metad_disp_id_metad ON symphonie.tj_a_metadonnee_disp_meta_metad USING BTREE (metad_disp_id);
	COMMENT ON INDEX symphonie.x_btr_metad_disp_id_metad IS
	'Index sur le rattachement au dispositif afin d''optimiser le temps des requètes';
	
	-- TABLE : a pour metadonnee - element
	CREATE TABLE symphonie.tj_a_metadonnee_elem_meta_metae (
		metae_meta_id		INT4				NULL,
		metae_elem_id		INT4				NULL,
		metae_valeur		VARCHAR(100)		NOT NULL,
		metae_date			DATE				NULL,
		metae_remarque		TEXT				NULL,
		CONSTRAINT tj_a_metadonnee_elem_meta_metae_pkey PRIMARY KEY (metae_meta_id,metae_elem_id),
		CONSTRAINT c_fk_meta_metae FOREIGN KEY (metae_meta_id)
			REFERENCES symphonie.t_metadonnee_meta(meta_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_elem_metae FOREIGN KEY (metae_elem_id)
			REFERENCES symphonie.t_element_elem(elem_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_metadonnee_elem_meta_metae IS
	'Table associant des métadonnée à un dispositif, à un élément (individu), à une mesure ou à un regroupement de mesures.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_elem_meta_metae.metae_meta_id IS
	'Association d''une métadonnée.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_elem_meta_metae.metae_elem_id IS
	'Association d''une métadonnée à un élément (individu).';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_elem_meta_metae.metae_valeur IS
	'Valeur de la métadonnée.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_elem_meta_metae.metae_date IS
	'Date à laquelle la métadonnée a été associée au dispositif.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_elem_meta_metae.metae_remarque IS
	'Remarque concernant l''association de la métadonnée au dispositif.';
	COMMENT ON CONSTRAINT c_fk_meta_metae ON symphonie.tj_a_metadonnee_elem_meta_metae IS
	'Rattachement d''une métadonnée à une valeur de métadonnée.';
	COMMENT ON CONSTRAINT c_fk_elem_metae ON symphonie.tj_a_metadonnee_elem_meta_metae IS
	'Rattachement d''un élément (individu) à une valeur de métadonnée.';
	ALTER TABLE symphonie.tj_a_metadonnee_elem_meta_metae OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_metae_meta_id_metae
	CREATE INDEX x_btr_metae_meta_id_metae ON symphonie.tj_a_metadonnee_elem_meta_metae USING BTREE (metae_meta_id);
	COMMENT ON INDEX symphonie.x_btr_metae_meta_id_metae IS
	'Index sur le rattachement a la métadonnée afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_metae_elem_id_metae
	CREATE INDEX x_btr_metae_elem_id_metae ON symphonie.tj_a_metadonnee_elem_meta_metae USING BTREE (metae_elem_id);
	COMMENT ON INDEX symphonie.x_btr_metae_elem_id_metae IS
	'Index sur le rattachement a l''élément (individu) afin d''optimiser le temps des requètes';
	
	-- TABLE : a pour metadonnee - mesure
	CREATE TABLE symphonie.tj_a_metadonnee_mes_meta_metam (
		metam_meta_id		INT4				NULL,
		metam_mes_id		INT4				NULL,
		metam_valeur		VARCHAR(100)		NOT NULL,
		metam_date			DATE				NULL,
		metam_remarque		TEXT				NULL,
		CONSTRAINT tj_a_metadonnee_mes_meta_metam_pkey PRIMARY KEY (metam_meta_id,metam_mes_id),
		CONSTRAINT c_fk_meta_metam FOREIGN KEY (metam_meta_id)
			REFERENCES symphonie.t_metadonnee_meta(meta_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_mes_metam FOREIGN KEY (metam_mes_id)
			REFERENCES symphonie.t_mesure_mes(mes_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_metadonnee_mes_meta_metam IS
	'Table associant des métadonnée à un dispositif, à un élément (individu), à une mesure ou à un regroupement de mesures.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_mes_meta_metam.metam_meta_id IS
	'Association d''une métadonnée.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_mes_meta_metam.metam_mes_id IS
	'Association d''une métadonnée à une mesure.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_mes_meta_metam.metam_valeur IS
	'Valeur de la métadonnée.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_mes_meta_metam.metam_date IS
	'Date à laquelle la métadonnée a été associée au dispositif.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_mes_meta_metam.metam_remarque IS
	'Remarque concernant l''association de la métadonnée au dispositif.';
	COMMENT ON CONSTRAINT c_fk_meta_metam ON symphonie.tj_a_metadonnee_mes_meta_metam IS
	'Rattachement d''une métadonnée à une valeur de métadonnée.';
	COMMENT ON CONSTRAINT c_fk_mes_metam ON symphonie.tj_a_metadonnee_mes_meta_metam IS
	'Rattachement d''une mesure à une valeur de métadonnée.';
	ALTER TABLE symphonie.tj_a_metadonnee_mes_meta_metam OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_metam_meta_id_metam
	CREATE INDEX x_btr_metam_meta_id_metam ON symphonie.tj_a_metadonnee_mes_meta_metam USING BTREE (metam_meta_id);
	COMMENT ON INDEX symphonie.x_btr_metam_meta_id_metam IS
	'Index sur le rattachement a la métadonnée afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_metam_mes_id_metam
	CREATE INDEX x_btr_metam_mes_id_metam ON symphonie.tj_a_metadonnee_mes_meta_metam USING BTREE (metam_mes_id);
	COMMENT ON INDEX symphonie.x_btr_metam_mes_id_metam IS
	'Index sur le rattachement a la mesure afin d''optimiser le temps des requètes';
	
	-- TABLE : a pour metadonnee - regroupement de mesure
	CREATE TABLE symphonie.tj_a_metadonnee_rgpm_meta_metar (
		metar_meta_id		INT4				NULL,
		metar_rgpm_id		INT4				NULL,
		metar_valeur		VARCHAR(100)		NOT NULL,
		metar_date			DATE				NULL,
		metar_remarque		TEXT				NULL,
		CONSTRAINT tj_a_metadonnee_rgpm_meta_metar_pkey PRIMARY KEY (metar_meta_id,metar_rgpm_id),
		CONSTRAINT c_fk_meta_metar FOREIGN KEY (metar_meta_id)
			REFERENCES symphonie.t_metadonnee_meta(meta_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_rgpm_metar FOREIGN KEY (metar_rgpm_id)
			REFERENCES symphonie.t_regroupement_mesure_rgpm(rgpm_id)
			ON DELETE CASCADE
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_metadonnee_rgpm_meta_metar IS
	'Table associant des métadonnée à un dispositif, à un élément (individu), à une mesure ou à un regroupement de mesures.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_rgpm_meta_metar.metar_meta_id IS
	'Association d''une métadonnée.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_rgpm_meta_metar.metar_rgpm_id IS
	'Association d''une métadonnée à un regroupement de mesure.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_rgpm_meta_metar.metar_valeur IS
	'Valeur de la métadonnée.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_rgpm_meta_metar.metar_date IS
	'Date à laquelle la métadonnée a été associée au dispositif.';
	COMMENT ON COLUMN symphonie.tj_a_metadonnee_rgpm_meta_metar.metar_remarque IS
	'Remarque concernant l''association de la métadonnée au dispositif.';
	COMMENT ON CONSTRAINT c_fk_meta_metar ON symphonie.tj_a_metadonnee_rgpm_meta_metar IS
	'Rattachement d''une métadonnée à une valeur de métadonnée.';
	COMMENT ON CONSTRAINT c_fk_rgpm_metar ON symphonie.tj_a_metadonnee_rgpm_meta_metar IS
	'Rattachement d''un regroupement de mesure à une métadonnée.';
	ALTER TABLE symphonie.tj_a_metadonnee_rgpm_meta_metar OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_metar_meta_id_metar
	CREATE INDEX x_btr_metar_meta_id_metar ON symphonie.tj_a_metadonnee_rgpm_meta_metar USING BTREE (metar_meta_id);
	COMMENT ON INDEX symphonie.x_btr_metar_meta_id_metar IS
	'Index sur le rattachement a la métadonnée afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_metar_rgpm_id_metar
	CREATE INDEX x_btr_metar_rgpm_id_metar ON symphonie.tj_a_metadonnee_rgpm_meta_metar USING BTREE (metar_rgpm_id);
	COMMENT ON INDEX symphonie.x_btr_metar_rgpm_id_metar IS
	'Index sur le rattachement au regroupement de mesures afin d''optimiser le temps des requètes';

	-- TABLE : a droit
	CREATE TABLE symphonie.tj_a_droit_ope_dro_opedro (
		opedro_ope_id		INT4		NOT NULL,
		opedro_dro_id 		INT4		NOT NULL,
		CONSTRAINT tj_a_droit_ope_dro_opedro_pkey PRIMARY KEY (opedro_ope_id,opedro_dro_id),
		CONSTRAINT c_fk_ope_opedro FOREIGN KEY (opedro_ope_id)
			REFERENCES symphonie.t_operateur_ope(ope_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE,
		CONSTRAINT c_fk_droi_opedro FOREIGN KEY (opedro_dro_id)
			REFERENCES symphonie.t_droit_dro(dro_id)
			ON DELETE NO ACTION
			ON UPDATE NO ACTION
			NOT DEFERRABLE
	);
	COMMENT ON TABLE symphonie.tj_a_droit_ope_dro_opedro IS
	'Table associant l''opérateur et les droits.';
	COMMENT ON COLUMN symphonie.tj_a_droit_ope_dro_opedro.opedro_ope_id IS
	'Association d''un opérateur avec un droit.';
	COMMENT ON COLUMN symphonie.tj_a_droit_ope_dro_opedro.opedro_dro_id IS
	'Association d''un droit avec un opérateur.';
	COMMENT ON CONSTRAINT c_fk_ope_opedro ON symphonie.tj_a_droit_ope_dro_opedro IS
	'Rattachement d''un opérateur a un droit.';
	COMMENT ON CONSTRAINT c_fk_droi_opedro ON symphonie.tj_a_droit_ope_dro_opedro IS
	'Rattachement d''un droit a un opérateur.';
	ALTER TABLE symphonie.tj_a_droit_ope_dro_opedro OWNER TO :proprietaire;
	
	-- création de l'Index : x_btr_opedro_opedro_ope_id_opedro
	CREATE INDEX x_btr_opedro_opedro_ope_id_opedro ON symphonie.tj_a_droit_ope_dro_opedro USING BTREE (opedro_ope_id);
	COMMENT ON INDEX symphonie.x_btr_opedro_opedro_ope_id_opedro IS
	'Index sur le rattachement a l''opérateur afin d''optimiser le temps des requètes';
	
	-- création de l'Index : x_btr_x_btr_opedro_opedro_dro_id_opedro
	CREATE INDEX x_btr_x_btr_opedro_opedro_dro_id_opedro ON symphonie.tj_a_droit_ope_dro_opedro USING BTREE (opedro_dro_id);
	COMMENT ON INDEX symphonie.x_btr_x_btr_opedro_opedro_dro_id_opedro IS
	'Index sur le rattachement au droit afin d''optimiser le temps des requètes';

-- ***************************
-- ***** création des vues ***
-- ***************************

	-- vue : vf_genotype_vgen
	CREATE VIEW symphonie.vj_a_convention_disp_conv_aconv AS
		SELECT 
			aconv_disp_id,
			aconv_conv_id,
			CASE WHEN conv_avenant=false THEN 'Convention' WHEN conv_avenant=true THEN 'Avenant' END AS type, conv_nom, conv_debut, conv_fin, conv_url, conv_commentaire
		FROM symphonie.t_convention_conv
		INNER JOIN symphonie.tj_a_convention_disp_conv_aconv ON aconv_conv_id = conv_id;
	
	ALTER VIEW symphonie.vj_a_convention_disp_conv_aconv OWNER TO :proprietaire;

	-- vue : v_symphonie_fichiers
	CREATE VIEW symphonie.v_symphonie_fichiers AS
		SELECT
			fichiers.fic_nom AS nom,
			fichiers.fic_version_publiee AS version,
			datatype.typf_nom AS datatype,
			fichiers.fic_ids_id as dataset,
			fichiers.fic_publie AS publie,
			fichiers.fic_id AS identifiant,
			fichiers.fic_validation_date AS valide_le,
			fichiers.fic_ope_id_validation AS valide_par,
			fichiers.fic_publication_date AS publie_le,
			publieur.ope_id AS publie_par,
			fichiers.fic_blob AS blob
		FROM symphonie.t_fichier_fic AS fichiers
		INNER JOIN symphonie.t_type_fichier_typf AS datatype ON datatype.typf_id = fichiers.fic_typf_id
		LEFT JOIN symphonie.t_operateur_ope AS publieur ON publieur.ope_id = fichiers.fic_ope_id_publication;

	ALTER VIEW symphonie.v_symphonie_fichiers OWNER TO :proprietaire;

	-- vue : v_symphonie_fichiers_en attentes
	CREATE VIEW symphonie.v_symphonie_fichiers_en_attente AS
		SELECT
			fichiers.ficope_nom_fichier AS nom,
			fichiers.ficope_version AS version,
			typf.typf_nom AS datatype,
			fichiers.ficope_ids_id AS dataset,
			CASE WHEN usr.login IS NULL THEN false ELSE true END AS publie,
			usr.login AS publie_par,
			dataset.ids_publish_date AS publie_le,
			fichiers.ficope_ope_id AS initie_par,
			fichiers.ficope_date AS initie_le,
			fichiers.ficope_type_operation AS type_operation,
			fichiers.ficope_statut AS statut_operation,
			fichiers.ficope_id AS identifiant,
			fichiers.ficope_fic_id AS identifiant_fichier,
			fichiers.ficope_operation_id AS num_operation
		FROM symphonie.t_fichier_operation_ficope as fichiers
		INNER JOIN symphonie.t_type_fichier_typf AS typf ON typf.typf_id = fichiers.ficope_typf_id
		INNER JOIN noyau.vf_noyau_insertion_dataset_ids AS dataset ON ids_id = fichiers.ficope_ids_id
		LEFT JOIN noyau.vf_noyau_utilisateur AS usr ON dataset.ids_publish_user = usr.id
		WHERE fichiers.ficope_statut = 'en attente';

	ALTER VIEW symphonie.v_symphonie_fichiers_en_attente OWNER TO :proprietaire;

	CREATE VIEW symphonie.v_dispositif_libelle AS
		SELECT DISTINCT
		disp_id,
        CASE
            WHEN com.com_id IS NOT NULL THEN concat(disp.disp_code_experience, ' (', disp.disp_nom, ')', ' - ', com.com_nom)
            WHEN pays.pays_id IS NOT NULL THEN concat(disp.disp_code_experience, ' (', disp.disp_nom, ')', ' - ', upper(pays.pays_nom::text))
            ELSE concat(disp.disp_code_experience, ' (', disp.disp_nom, ')')
        END AS libelle
		FROM symphonie.t_dispositif_disp disp
		LEFT JOIN symphonie.tj_localisation_disp_com_loc loc ON loc.loc_disp_id = disp.disp_id
		LEFT JOIN symphonie.tj_a_localisation_disp_pays_aloc aloc ON aloc.aloc_disp_id = disp.disp_id
		LEFT JOIN symphonie.tr_communes_com com ON com.com_id = loc.loc_com_id
		LEFT JOIN symphonie.tr_pays_pays pays ON pays.pays_id = aloc.aloc_pays_id
		ORDER BY libelle ASC;
	
	ALTER VIEW symphonie.v_dispositif_libelle OWNER TO :proprietaire;

	CREATE VIEW symphonie.v_bornes_geom AS
		WITH nb_element AS (
			SElECT 
				disp_id,
				COUNT(elem_id) AS nb_elements 
			FROM symphonie.t_dispositif_disp 
			INNER JOIN symphonie.t_element_elem ON elem_disp_id = disp_id
			GROUP BY disp_id
		)
		SELECT
			bor_id,
			disp.disp_id AS identifiant,
			disp_code_experience AS code_experience,
			nb_elements,
			COUNT(DISTINCT rgpm_id) AS nb_variables,
			string_agg(DISTINCT esp_nom, ', ') AS espece_presente,
			acti_libelle AS activite,
			symphonie.t_borne_bor.geom
		FROM symphonie.t_dispositif_disp disp
		LEFT JOIN symphonie.t_regroupement_mesure_rgpm ON rgpm_disp_id = disp_id
		LEFT JOIN symphonie.tj_espece_presente_disp_esp_espp ON espp_disp_id = disp_id
		LEFT JOIN symphonie.tr_espece_esp ON esp_id = espp_esp_id
		LEFT JOIN symphonie.t_activite_acti ON acti_id = disp_acti_id
		INNER JOIN symphonie.t_borne_bor ON bor_disp_id = disp_id
		LEFT JOIN nb_element ON nb_element.disp_id =  disp.disp_id
		GROUP BY (bor_id, disp.disp_id, disp_code_experience, nb_element.nb_elements, acti_libelle, symphonie.t_borne_bor.geom);
	
	ALTER VIEW symphonie.v_bornes_geom OWNER TO :proprietaire;
COMMIT;